﻿using System.Collections.Generic;

namespace Calcon.ModelCalsses.Models
{
    public class DepartmentModel : BaseModel
    {
        public string Name { get; set; }

        public CompanyModel Company { get; set; }
        public IEnumerable<EmployeeModel> Employees { get; set; }
    }
}
