﻿using System;
using Calcon.ModelCalsses.Enums;
using Calcon.ModelCalsses.ValueType;

namespace Calcon.ModelCalsses.Models
{
    public class EmployeeModel : BaseModel
    {
        //[Length(Min = 3, Max = 20,Message="Between 3 and 20")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }

        //[Length(Min = 3, Max = 60, Message = "Between 3 and 60")]
        public string LastName { get; set; }
        public GenderModel Gender { get; set; }
        public DateTime? Birthdate { get; set; }
        public AddressModel Address { get; set; }
        public string Tel { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public DepartmentModel Department { get; set; }
    }
}
