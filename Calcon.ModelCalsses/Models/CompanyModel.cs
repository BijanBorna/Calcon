﻿using System.Collections.Generic;
using Calcon.ModelCalsses.ValueType;

namespace Calcon.ModelCalsses.Models
{
    public class CompanyModel : BaseModel
    {
        public string Name { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public AddressModel Address { get; set; }

        public IEnumerable<DepartmentModel> Departments { get; set; }
    }
}
