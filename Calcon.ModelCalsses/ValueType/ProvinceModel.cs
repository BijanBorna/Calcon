﻿using System;
using Calcon.Framework.Core.Application.Models;

namespace Calcon.ModelCalsses.ValueType
{
    public class ProvinceModel : IBaseStructur
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CountryModel Country { get; set; }
    }
}
