﻿using System;
using System.Collections.Generic;
using Calcon.Framework.Core.Application.Models;

namespace Calcon.ModelCalsses.ValueType
{
    public class CountryModel : IBaseStructur
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<ProvinceModel> Provinces { get; set; }
    }
}
