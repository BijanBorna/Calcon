﻿using System;
using Calcon.Framework.Core.Application.Models;

namespace Calcon.ModelCalsses.ValueType
{
    public class AddressModel : IBaseStructur
    {
        public Guid Id { get; set; }
        public string Place { get; set; }
        public string ZipCode { get; set; }
        public CountryModel Country { get; set; }
        public ProvinceModel Province { get; set; }
    }
}
