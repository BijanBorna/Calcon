﻿namespace Calcon.ModelCalsses.ValueType
{
    public struct ErrorModel
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}
