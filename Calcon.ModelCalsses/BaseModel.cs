﻿using System;
using Calcon.Framework.Core.Adapter;

namespace Calcon.ModelCalsses
{
    public class BaseModel : IParam
    {
        public Guid Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModifyDate { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
