﻿using System.Data;
using System.Web;
using Calcon.Framework.Data.NHSessionManager;
using Calcon.Infrastructure.Tasks;
using NHibernate;

namespace Calcon.Infrastructure
{
    public class TransactionPerRequestTask : IRunOnBeginRequestTask, IRunOnErrorTask, IRunOnEndRequestTask
    {
        #region Constructor

        public TransactionPerRequestTask(HttpContextBase httpContext)
        {
            _session = SingletonCore.SessionFactory.OpenSession();
            _httpContext = httpContext;
        }

        #endregion

        #region Constants

        private const string Error = "TRANSACTION_PER_REQUEST_ERROR_KEY";
        private const string Transaction = "TRANSACTION_PER_REQUEST_TRANSACTION_KEY";

        #endregion

        #region Fields

        private readonly ISession _session;
        private readonly HttpContextBase _httpContext;

        #endregion

        #region Methods

        int IRunOnBeginRequestTask.Order => int.MaxValue;

        int IRunOnErrorTask.Order => int.MaxValue;

        int IRunOnEndRequestTask.Order => int.MaxValue;

        void IRunOnBeginRequestTask.Execute()
        {
            _httpContext.Items[Transaction] =
                _session.BeginTransaction(IsolationLevel.Snapshot);
        }

        void IRunOnErrorTask.Execute()
        {
            _httpContext.Items[Error] = true;
        }

        void IRunOnEndRequestTask.Execute()
        {
            var transaction = (ITransaction) _httpContext.Items[Transaction];
            if (_httpContext.Items[Error] != null)
                transaction.Rollback();
            else
                transaction.Commit();
        }

        #endregion
    }
}