﻿namespace Calcon.Infrastructure.Tasks
{
    public interface IRunOnEndRequestTask
    {
        int Order { get; }
        void Execute();
    }
}