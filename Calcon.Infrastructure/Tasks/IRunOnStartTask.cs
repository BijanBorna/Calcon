﻿namespace Calcon.Infrastructure.Tasks
{
    public interface IRunOnStartTask
    {
        int Order { get; }
        void Execute();
    }
}