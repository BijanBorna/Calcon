﻿namespace Calcon.Infrastructure.Tasks
{
    public interface IRunOnEndTask
    {
        int Order { get; }
        void Execute();
    }
}