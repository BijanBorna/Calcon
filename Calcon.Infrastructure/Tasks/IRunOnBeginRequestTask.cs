﻿namespace Calcon.Infrastructure.Tasks
{
    public interface IRunOnBeginRequestTask
    {
        int Order { get; }
        void Execute();
    }
}