﻿namespace Calcon.Infrastructure.Tasks
{
    public interface IRunOnOwinStartupTask
    {
        int Order { get; }
        void Execute();
    }
}