﻿namespace Calcon.Infrastructure.Tasks
{
    public interface IRunOnErrorTask
    {
        int Order { get; }
        void Execute();
    }
}