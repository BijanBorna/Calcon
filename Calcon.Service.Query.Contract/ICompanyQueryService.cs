﻿using System;
using System.Collections.Generic;
using Calcon.ViewModelClasses.PublicModels;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Query.Contract
{
    public interface ICompanyQueryService : IQueryService
    {
        CompanyViewModel Get(Guid id);
        List<CompanyViewModel> GetAll();
        List<TreeViewModel> GetForTree();
        IEnumerable<ComboViewModel> GetForCombo();
    }
}
