﻿using Calcon.Common;

namespace Calcon.Service.Query.Contract
{
    public interface IUserQueryService : IQueryService
    {
        MembershipContext ValidateUser(string email, string password);
    }
}
