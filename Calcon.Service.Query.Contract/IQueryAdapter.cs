﻿namespace Calcon.Service.Query.Contract
{
    public interface IQueryAdapter<in TSource,out TDestination>
    {
        TDestination ToMap(TSource source);
    }
}
