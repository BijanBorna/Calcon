﻿using System;
using System.Collections.Generic;
using Calcon.ViewModelClasses.PublicModels;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Query.Contract
{
    public interface IEmployeeQueryService : IQueryService
    {
        EmployeeViewModel Get(Guid id);
        List<EmployeeViewModel> GetAll();
        List<TreeViewModel> GetForTree(Guid departmentId);
    }
}
