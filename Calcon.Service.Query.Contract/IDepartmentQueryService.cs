﻿using System;
using System.Collections.Generic;
using Calcon.ViewModelClasses.PublicModels;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Query.Contract
{
    public interface IDepartmentQueryService : IQueryService
    {
        DepartmentViewModel Get(Guid id);
        List<DepartmentViewModel> GetAll();
        List<TreeViewModel> GetForTree(Guid comapnyId);
        IEnumerable<ComboViewModel> GetForCombo();
        IEnumerable<ComboViewModel> GetForCombo(Guid comapnyId);
    }
}
