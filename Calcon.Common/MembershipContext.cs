﻿using System.Security.Principal;
using Calcon.ViewModelClasses.ViewModels.UserViewModel;

namespace Calcon.Common
{
    public class MembershipContext
    {
        public IPrincipal Principal { get; set; }
        public UserViewModel User { get; set; }

        public bool IsValid()
        {
            return Principal != null;
        }
    }
}
