﻿using System;

namespace Calcon.Common.Aspects.Transaction
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class TransactionalAttribute : Attribute
    {

    }
}
