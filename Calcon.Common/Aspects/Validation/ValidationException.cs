﻿using System;
using System.Collections.Generic;
using Calcon.ModelCalsses.ValueType;

namespace Calcon.Common.Aspects.Validation
{
    [Serializable()]
    public class ValidationException : Exception
    {
        public IList<ErrorModel> Errors { get; }
        public ValidationException(string message = "")
            : base(message)
        {
            Errors = new List<ErrorModel>();
            AddError(message);
        }

        public void AddError(string message)
        {
            if (string.IsNullOrWhiteSpace(message) || string.IsNullOrEmpty(message))
                return;
            Errors.Add(new ErrorModel { Message = message });
        }
        public void AddError(string key, string message)
        {
            Errors.Add(new ErrorModel{Name = key, Message = message});
        }
        public void AddError<T>(Func<T, object> key, string message)
        {
            Errors.Add(new ErrorModel { Name = key.GetType().Name, Message = message });
        }
    }
}
