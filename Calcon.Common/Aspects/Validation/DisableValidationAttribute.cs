using System;

namespace Calcon.Common.Aspects.Validation
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class )]
    public sealed class DisableValidationAttribute : Attribute
    {

    }
}