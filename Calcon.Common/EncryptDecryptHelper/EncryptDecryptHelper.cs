﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Calcon.Common.EncryptDecryptHelper
{
    public static class EncryptDecryptHelper
    {
        public static byte[] EncryptBytes(string StringToEncrypt)
        {
            byte[] StringToEncryptBytes = Encoding.UTF8.GetBytes(StringToEncrypt);
            MemoryStream memoryStream = new MemoryStream();
            // Define cryptographic stream (always use Write mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,ASICryptoTransform.GetEncryptor(),CryptoStreamMode.Write);
            // Start encrypting.
            cryptoStream.Write(StringToEncryptBytes, 0, StringToEncryptBytes.Length);
            // Finish encrypting.
            // Convert our encrypted data from a memory stream into a byte array.
            byte[] cipherTextBytes = memoryStream.ToArray();
            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();
            return cipherTextBytes;
        }
        public static string Encrypt(string StringToEncrypt)
        {
            return Convert.ToBase64String(EncryptBytes(StringToEncrypt));
        }
        public static byte[] DecryptBytes(byte[] StringToDecryptBytes)
        {
            //byte[] StringToDecryptBytes = Convert.FromBase64String(StringToDecrypt);
            MemoryStream memoryStream = new MemoryStream(StringToDecryptBytes);
            // Define cryptographic stream (always use Read mode for encryption).
            CryptoStream cryptoStream = new CryptoStream(memoryStream,ASICryptoTransform.GetDecryptor(),CryptoStreamMode.Read);
            // Since at this point we don't know what the size of decrypted data
            // will be, allocate the buffer long enough to hold ciphertext;
            // plaintext is never longer than ciphertext.
            byte[] plainTextBytes = new byte[StringToDecryptBytes.Length];
            // Start decrypting.
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            // Close both streams.
            memoryStream.Close();
            cryptoStream.Close();
            // Convert decrypted data into a string. 
            // Let us assume that the original plaintext string was UTF8-encoded.
            string plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
            return Encoding.UTF8.GetBytes(plainText);
        }
        public static byte[] DecryptBytes(string StringToDecrypt)
        {
            byte[] StringToDecryptBytes = Convert.FromBase64String(StringToDecrypt);
            return DecryptBytes(StringToDecryptBytes);
        }
        public static string Decrypt(string StringToDecrypt)
        {
            return Encoding.ASCII.GetString(DecryptBytes(StringToDecrypt));
        }
        public static string Decrypt(byte[] ByteArrayToDecrypt)
        {
            return Encoding.ASCII.GetString(DecryptBytes(ByteArrayToDecrypt));
        }
        #region Intrnal Class
        internal static class ASICryptoTransform
        {
            private static string passPhrase = "#$^&*!@!$";        // can be any string
            private static string saltValue = "R@j@}{BAe";        // can be any string
            private static string hashAlgorithm = "MD5";             // can be "MD5"
            private static int passwordIterations = 2;                  // can be any number
            private static string initVector = "@1B2c3D4e5F6g7H8"; // must be 16 bytes
            private static int keySize = 256;                // can be 192 or 128
            private static RijndaelManaged symmetricKey = new RijndaelManaged();
            private static byte[] GetkeyBytes()
            {
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
                PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase,saltValueBytes,hashAlgorithm,passwordIterations);
                return password.GetBytes(keySize / 8);
            }
            public static ICryptoTransform GetEncryptor()
            {
                symmetricKey.Mode = CipherMode.CBC;
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                return symmetricKey.CreateEncryptor(GetkeyBytes(),initVectorBytes);
            }
            public static ICryptoTransform GetDecryptor()
            {
                symmetricKey.Mode = CipherMode.CBC;
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                return symmetricKey.CreateDecryptor(GetkeyBytes(),initVectorBytes);
            }
        }
        #endregion
    }
}
