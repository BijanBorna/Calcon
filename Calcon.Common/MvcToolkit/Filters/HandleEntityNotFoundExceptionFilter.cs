﻿using System.Web.Mvc;
using Calcon.Framework.Core.Domain.Entities;

namespace Calcon.Common.MvcToolkit.Filters
{
    public sealed class HandleEntityNotFoundExceptionFilter : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var entityNotFoundException = filterContext.Exception as EntityNotFoundException;

            if (entityNotFoundException == null) return;

            filterContext.Result = new HttpNotFoundResult();

            filterContext.ExceptionHandled = true;
        }
    }
}