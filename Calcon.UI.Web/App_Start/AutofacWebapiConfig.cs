﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Calcon.Service.Command.Config;
using Calcon.Service.Query.Config;
//using FluentValidation;

namespace Calcon.UI.Web
{
    public class AutofacWebapiConfig
    {
        public static Autofac.IContainer Container;
        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }
        public static void Initialize(HttpConfiguration config, Autofac.IContainer container)
        {
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
        private static Autofac.IContainer RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules(Assembly.GetExecutingAssembly());

            Container = builder.Build();
            return Container;
        }
    }

    public class AutofacWebApiConfig : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterAssemblyModules(typeof(AutofacCommandServiceConfig).Assembly);
            builder.RegisterAssemblyModules(typeof(AutofacQueryServiceConfig).Assembly);
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            //// Other Setting
            //var container = builder.Build();
            //AutofacWebApiDependencyResolver.SetContainer(container);
            //DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }
    }
}