﻿using System.Web.Optimization;

namespace Calcon.UI.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            #region [Common]
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                       "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/vendors").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/toastr.js",
                        "~/Scripts/respond.src.js",
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-route.js",
                        "~/Scripts/angular-cookies.js",
                        "~/Scripts/angular-validator.js",
                        "~/Scripts/angular-base64.js",
                        "~/Scripts/angular-file-upload.js",
                        "~/Scripts/angular-resource.js",
                        "~/Scripts/angucomplete-alt.min.js",
                        "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                        "~/Scripts/jquery.fancybox.js",
                        "~/Scripts/jquery.fancybox-media.js",
                        "~/Scripts/loading-bar.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/common/app").Include(
               "~/app/services/apiService.js",
               "~/app/services/helperService.js",
               "~/app/services/notificationService.js",
               "~/app/services/membershipService.js",
               "~/app/services/fileUploadService.js"
               ));
            #endregion
            #region [App]
            bundles.Add(new ScriptBundle("~/bundles/app").Include(
               "~/app/app.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/component").Include(
              "~/app/components/home/rootCtrl.js",
               "~/app/components/layout/topBar.directive.js",
                "~/app/components/layout/sideBar.directive.js",
                "~/app/components/account/loginCtrl.js",
                "~/app/components/account/registerCtrl.js",
                "~/app/components/home/indexCtrl.js",
                "~/app/components/company/companyCtrl.js",
                "~/app/components/department/departmentCtrl.js",
                "~/app/components/employee/employeeCtrl.js",
                "~/Scripts/angular-linq.min.js",
                "~/app/directives/nivoslider/jquery.nivo.slider.js",
                "~/app/directives/nivoslider/nivoslider.directive.js",
                "~/bower_components/angular-ui-tree/dist/angular-ui-tree.js"
                ));
            #endregion
            #region [Common Css]
            bundles.Add(new StyleBundle("~/Content/css").Include(
              "~/content/bootstrap.css",
              "~/content/bootstrap-theme-rtl.css",
               "~/content/font-awesome.css",
              "~/content/toastr.css",
              "~/content/jquery.fancybox.css",
              "~/content/loading-bar.css",
              "~/content/site.css",
              "~/bower_components/angular-ui-tree/dist/angular-ui-tree.css"
              ));
            #endregion
            #region [App Css]
            bundles.Add(new StyleBundle("~/AppContent/css").Include(
              "~/app/directives/nivoslider/default.css",
              "~/app/directives/nivoslider/nivo-slider.css"
             ));
            #endregion

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
                BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
