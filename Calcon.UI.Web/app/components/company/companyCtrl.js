﻿(function (app) {
    'use strict';

    app.controller('companyCtrl', companyCtrl);

    companyCtrl.$inject = ['$scope', '$location', '$routeParams', 'apiService', 'notificationService'];

    function companyCtrl($scope, $location, $routeParams, apiService, notificationService) {
        $scope.company = {};
        $scope.loadingcompany = true;
        $scope.isReadOnly = false;
        $scope.updatecompany = updatecompany;

        var uiMode = {
            InsertMode: 1,
            UpdateMode: 2
        };

        $scope.mode = ($scope.params && $scope.params.Id) ? uiMode.UpdateMode : uiMode.InsertMode;
        $scope.modeName = ($scope.params && $scope.params.Id) ? 'Update' : 'Insert';

        function loadCompany() {
            var id = null;

            if ($routeParams.id != undefined) {
                var queryParam = $routeParams.id.split(",");
                if (queryParam.length > 1) {
                    id = queryParam[1];
                }
            }

            if (id != null) {
                apiService.get('/api/Company/Get/' + id)
                    .then(function (result) {
                        $scope.company = result.data;
                        $scope.mode = uiMode.UpdateMode;
                        $scope.modeName = 'Update';
                        $scope.loadingcompany = false;
                    }, function () {
                        $scope.loadingcompany = false;
                    });
            } else
                $scope.loadingcompany = false;
        }
        loadCompany();

        function updatecompany() {
            updatecompanyModel();
        }

        function updatecompanyModel() {

            var url = "/api/Company/Insert/";
            if ($scope.mode == uiMode.UpdateMode) {
                url = "/api/Company/Update/";
            }

            apiService.save(url, $scope.company).then(updatecompanySucceded, updatecompanyFailed);
        }

        function updatecompanySucceded(response) {
            if ($scope.mode == uiMode.UpdateMode) {
                $scope.mode = uiMode.InsertMode;
                $scope.modeName = 'Insert';
                editNode(response);
            } else
                addNode(response);

            console.log(response);
            notificationService.displaySuccess($scope.company.Name + ' has been updated');
            $location.path('#/');
            $scope.company = {};
        }

        function updatecompanyFailed(response) {
            //notificationService.displayError(response);
        }

        function addNode(source) {
            $scope.$parent.addNode(getTreeModel(source));
        }
        function editNode(source) {
            $scope.$parent.editNode(getTreeModel(source));
        }
        function getTreeModel(source) {
            return {
                id: source.Id,
                title: source.Name,
                nodes: [],
                path: 'department',
                parentId: null,
                type: 'Company'
            };
        }
    }

})(angular.module('calconApp'));
