﻿(function(app) {
    'use strict';

    app.directive('topBar', topBar);

    topBar.$inject = ['membershipService'];

    function topBar(membershipService) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/app/components/layout/topBar.html',
            link: function(scope, element, attrs) {

                function logout() {
                    scope.userData.isUserLoggedIn = false;
                    membershipService.removeCredentials();
                    $location.path('#/');
                    $scope.userData.displayUserInfo();
                }
            }
        }
    }

})(angular.module('calconApp'));
