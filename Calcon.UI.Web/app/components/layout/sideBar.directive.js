﻿(function (app) {
    'use strict';

    app.directive('sideBar', sideBar);

    sideBar.$inject = ['apiService', '$location', '$linq', 'notificationService'];

    function sideBar(apiService, $location, $linq, notificationService) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/app/components/layout/sideBar.html',
            link: function (scope, element, attrs) {
                if (!scope.data)
                    scope.data = [];

                scope.toggle = function (scope) {
                    scope.toggle();
                };

                scope.moveLastToTheBeginning = function () {
                    var a = scope.data.pop();
                    scope.data.splice(0, 0, a);
                };

                scope.newItem = function () {
                    var url = '/company';
                    $location.path(url);
                };

                //scope.newSubItem = function (scope) {
                //    var url = '/' + scope.$modelValue.path +
                //        '/' + scope.$modelValue.id +
                //        (scope.$modelValue.parentId != null ? ',' + scope.$modelValue.parentId : '');
                //    $location.path(url);
                //};

                scope.newSubItem = function (scope) {
                    var model = scope.$modelValue;
                    var url = '/' + model.path + '/1' +
                        (model.id != null ? ',' + model.id : '') +
                        (model.parentId != null ? ',' + model.parentId : '');
                    $location.path(url);
                };

                scope.editItem = function (scope) {
                    var model = scope.$modelValue;
                    var url = '/' + model.type.toLowerCase() + '/2' +
                        (model.id != null ? ',' + model.id : '') +
                        (model.parentId != null ? ',' + model.parentId : '');
                    
                    $location.path(url);
                };

                scope.addNode = function (param) {
                    if (param.parentId == null) {
                        scope.data.push(param);
                    } else {
                        $linq.Enumerable()
                            .From(scope.data)
                            .Any(function (x) {
                                if (x.id == param.parentId)
                                    x.nodes.push(param);
                                return ($linq.Enumerable()
                                    .From(x.nodes)
                                    .Any(function (x) {
                                        if (x.id == param.parentId)
                                            x.nodes.push(param);
                                        return ($linq.Enumerable()
                                            .From(x.nodes)
                                            .Any(function (x) {
                                                if (x.id == param.parentId)
                                                    x.nodes.push(param);
                                                return;
                                            }));
                                    })
                                );
                            });
                    }
                };

                scope.editNode = function (param) {
                    $linq.Enumerable()
                        .From(scope.data)
                        .Any(function (x) {
                            if (x.id == param.id)
                                x.title = param.title;
                            return ($linq.Enumerable()
                                .From(x.nodes)
                                .Any(function (x) {
                                    if (x.id == param.id)
                                        x.title = param.title;
                                    return ($linq.Enumerable()
                                        .From(x.nodes)
                                        .Any(function (x) {
                                            if (x.id == param.id)
                                                x.title = param.title;
                                            return;
                                        }));
                                })
                            );
                        });
                };

                scope.delete = function (scope) {
                    var model = scope.$modelValue;
                    if (model.nodes && model.nodes.length > 0) {
                        notificationService.displayWarning("The node's " + model.title + ' has ' + model.nodes.length + ' children.');
                        return;
                    }

                    var url = '/api/' + model.type + '/Delete/' + model.id;
                    apiService.remove(url).then(
                        function () {
                            scope.remove();
                        }, function () {
                        });
                };

                scope.collapseAll = function () {
                    scope.$broadcast('angular-ui-tree:collapse-all');
                };

                scope.expandAll = function () {
                    scope.$broadcast('angular-ui-tree:expand-all');
                };

                scope.loadData = function (scope) {
                    getData();
                };

                scope.loadChildData = function (scope) {
                    getChildData(scope.$modelValue);
                };

                function getData() {
                    if (!scope.userData.isUserLoggedIn)
                        return;

                    apiService.getAll('/api/Company/GetForTree/')
                        .then(function (result) {
                            scope.data = result.data;

                            setTimeout(function () {
                                scope.collapseAll();
                            }, 2000);
                        }, function () {
                        });
                }
                function getChildData(param) {
                    apiService.getAllByParam('/api/' + param.path + '/GetForTree/', { parentId: param.id })
                        .then(function (result) {
                            param.nodes = result.data;
                        }, function () {
                        });
                }
                //getData();
            }
        }
    }

})(angular.module('calconApp'));
