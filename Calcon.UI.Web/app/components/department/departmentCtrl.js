﻿(function (app) {
    'use strict';

    app.controller('departmentCtrl', departmentCtrl);

    departmentCtrl.$inject = ['$scope', '$location', '$routeParams', 'apiService', 'notificationService'];

    function departmentCtrl($scope, $location, $routeParams, apiService, notificationService) {
        $scope.department = {};
        $scope.Companies = [];
        $scope.loadingdepartment = true;
        $scope.isReadOnly = false;
        $scope.updatedepartment = updatedepartment;

        var uiMode = {
            InsertMode: 1,
            UpdateMode: 2
        };

        $scope.mode = ($scope.params && $scope.params.Id) ? uiMode.UpdateMode : uiMode.InsertMode;
        $scope.modeName = ($scope.params && $scope.params.Id) ? 'Update' : 'Insert';

        function loadDepartment() {
            var id = null;

            if ($routeParams.id != undefined) {
                var queryParam = $routeParams.id.split(",");
                if (queryParam.length > 1) {
                    if (queryParam[0] == 1) {
                        $scope.department.CompanyId = queryParam[1];
                    } else {
                        $scope.department.CompanyId = queryParam[2];
                        id = queryParam[1];
                    }
                }
                //else
                //    $scope.department.CompanyId = $routeParams.id;
            }

            loadCompanies();


            if (id != null) {
                $scope.loadingdepartment = true;
                apiService.get('/api/Department/Get/' + id)
                    .then(function (result) {
                        $scope.department = result.data;
                        $scope.mode = uiMode.UpdateMode;
                        $scope.modeName = 'Update';
                        $scope.loadingdepartment = false;
                    }, function () {
                        $scope.loadingdepartment = false;
                    });
            } else
                $scope.loadingdepartment = false;

            //apiService.get('/api/Department/Get/' + $routeParams.id).then(function (result) {
            //    $scope.department = result.data;
            //    $scope.loadingdepartment = false;
            //}, function () {
            //});
        }

        function companiesLoadCompleted(response) {
            $scope.Companies = response.data;
        }

        function companiesLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function loadCompanies() {
            apiService.getAll('/api/Company/GetForCombo').then(
                companiesLoadCompleted,
                companiesLoadFailed);
        }


        function updatedepartment() {
            updatedepartmentModel();
        }

        function updatedepartmentModel() {

            var url = "/api/Department/Insert/";
            if ($scope.mode == uiMode.UpdateMode) {
                url = "/api/Department/Update/";
            }

            apiService.save(url, $scope.department).then(updatedepartmentSucceded, updatedepartmentFailed);
        }

        function updatedepartmentSucceded(response) {
            if ($scope.mode == uiMode.UpdateMode) {
                $scope.mode = uiMode.InsertMode;
                $scope.modeName = 'Insert';
                editNode(response);
            } else
                addNode(response);

            console.log(response);
            notificationService.displaySuccess($scope.department.Name + ' has been updated');
            $location.path('#/');
            $scope.department = {};
        }

        function addNode(source) {
            $scope.$parent.addNode(getTreeModel(source));
        }
        function editNode(source) {
            $scope.$parent.editNode(getTreeModel(source));
        }
        function getTreeModel(source) {
            return {
                id: source.Id,
                title: source.Name,
                nodes: [],
                path: 'employee',
                parentId: source.CompanyId,
                type: 'Department'
            };
        }

        function updatedepartmentFailed(response) {
            //notificationService.displayError(response);
        }

        loadDepartment();
    }

})(angular.module('calconApp'));
