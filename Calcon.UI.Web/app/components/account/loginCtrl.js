﻿(function (app) {
    'use strict';

    app.controller('loginCtrl', loginCtrl);

    loginCtrl.$inject = ['$scope', 'membershipService', 'notificationService', '$rootScope', '$location'];

    function loginCtrl($scope, membershipService, notificationService, $rootScope, $location) {
        $scope.pageClass = 'page-login';
        $scope.login = login;
        $scope.user = {};

        function pageLoading() {
            if ($scope.$parent.userData.isUserLoggedIn) {
                if ($rootScope.previousState)
                    $location.path($rootScope.previousState);
                else
                    $location.path('/');
            }
        }

        function login() {
            membershipService.login($scope.user).then(function (data) {
                $scope.user.firstname = data.data.FirstName;
                $scope.user.lastname = data.data.LastName;
                membershipService.saveCredentials($scope.user);
                notificationService.displaySuccess('Hello ' + data.data.FirstName + ' ' + data.data.LastName);
                $scope.userData.displayUserInfo();
                //$scope.$parent.loadData();
                //$scope.$parent.loadData();
                if ($rootScope.previousState)
                    $location.path($rootScope.previousState);
                else
                    $location.path('/');
            }, function (data) {
                notificationService.displayError('Login failed. Try again.');
            });
        }

        pageLoading();
    }

})(angular.module('calconApp'));