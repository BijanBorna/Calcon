﻿(function (app) {
    'use strict';

    app.controller('registerCtrl', registerCtrl);

    registerCtrl.$inject = ['$scope', 'membershipService', 'notificationService', '$rootScope', '$location'];

    function registerCtrl($scope, membershipService, notificationService, $rootScope, $location) {
        $scope.pageClass = 'page-login';
        $scope.register = register;
        $scope.user = {};

        function pageLoading() {
            if ($scope.$parent.userData.isUserLoggedIn) {
                if ($rootScope.previousState)
                    $location.path($rootScope.previousState);
                else
                    $location.path('/');
            }
        }

        function register() {
            membershipService.register($scope.user).then(function (data) {
                $scope.user.firstname = data.data.FirstName;
                $scope.user.lastname = data.data.LastName;
                membershipService.saveCredentials($scope.user);
                notificationService.displaySuccess('Hello ' + data.data.FirstName + ' ' + data.data.LastName);
                $scope.userData.displayUserInfo();
                $location.path('/');
            }, function (data) {
                notificationService.displayError('Registration failed. Try again.');
            });
        }

        pageLoading();
    }

})(angular.module('calconApp'));
