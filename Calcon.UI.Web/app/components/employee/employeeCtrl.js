﻿(function (app) {
    'use strict';

    app.controller('employeeCtrl', employeeCtrl);

    employeeCtrl.$inject = ['$scope', '$location', '$routeParams', 'apiService', 'notificationService'];

    function employeeCtrl($scope, $location, $routeParams, apiService, notificationService) {
        $scope.employee = {};
        $scope.Departments = [];
        $scope.loadingemployee = true;
        $scope.isReadOnly = false;
        $scope.updateemployee = updateemployee;
        $scope.openDatePicker = openDatePicker;

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 1
        };
        $scope.datepicker = {};

        var uiMode = {
            InsertMode: 1,
            UpdateMode: 2
        };

        $scope.mode = ($scope.params && $scope.params.Id) ? uiMode.UpdateMode : uiMode.InsertMode;
        $scope.modeName = ($scope.params && $scope.params.Id) ? 'Update' : 'Insert';

        var companyId = '';

        function loadEmployee() {
            var id = null;

            if ($routeParams.id != undefined) {
                var queryParam = $routeParams.id.split(",");
                if (queryParam.length > 1) {
                    if (queryParam[0] == 1) {
                        $scope.employee.DepartmentId = queryParam[1];
                        companyId = queryParam[2];
                    } else {
                        id = queryParam[1];
                        $scope.employee.DepartmentId = queryParam[2];
                    }
                }
            }

            loadDepartments();


            if (id != null) {
                $scope.loadingemployee = true;
                apiService.get('/api/Employee/Get/' + id)
                    .then(function (result) {
                        $scope.employee = result.data;
                        $scope.mode = uiMode.UpdateMode;
                        $scope.modeName = 'Update';
                        $scope.loadingemployee = false;
                    }, function () {
                        $scope.loadingemployee = false;
                    });
            } else
                $scope.loadingemployee = false;

            //apiService.get('/api/Employee/Get/' + $routeParams.id).then(function (result) {
            //    $scope.employee = result.data;
            //    $scope.loadingemployee = false;
            //}, function () {
            //});
        }

        function departmentsLoadCompleted(response) {
            $scope.Departments = response.data;
        }

        function departmentsLoadFailed(response) {
            notificationService.displayError(response.data);
        }

        function loadDepartments() {
            var url = '/api/Department/GetForCombo/';
            if (companyId.length > 30)
                url += companyId;

            apiService.getAll(url).then(departmentsLoadCompleted, departmentsLoadFailed);
        }

        function updateemployee() {
            updateemployeeModel();
        }

        function updateemployeeModel() {

            var url = "/api/Employee/Insert/";
            if ($scope.mode == uiMode.UpdateMode) {
                url = "/api/Employee/Update/";
            }

            apiService.save(url, $scope.employee).then(updateemployeeSucceded, updateemployeeFailed);
        }

        function updateemployeeSucceded(response) {
            if ($scope.mode == uiMode.UpdateMode) {
                $scope.mode = uiMode.InsertMode;
                $scope.modeName = 'Insert';
                editNode(response);
            } else
                addNode(response);

            console.log(response);
            notificationService.displaySuccess($scope.employee.FirstName + ' ' + $scope.employee.LastName + ' has been updated');
            $location.path('#/');
            $scope.employee = {};
        }

        function updateemployeeFailed(response) {
            //notificationService.displayError(response);
        }

        function addNode(source) {
            $scope.$parent.addNode(getTreeModel(source));
        }
        function editNode(source) {
            $scope.$parent.editNode(getTreeModel(source));
        }
        function getTreeModel(source) {
            return {
                id: source.Id,
                title: source.FirstName + ' ' + source.LastName,
                nodes: null,
                path: null,
                parentId: source.DepartmentId,
                type: 'Employee',
                isLast: true
            };
        }

        function openDatePicker($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepicker.opened = true;
        };

        loadEmployee();
    }

})(angular.module('calconApp'));
