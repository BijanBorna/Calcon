﻿(function (app) {
    'use strict';

    app.controller('indexCtrl', indexCtrl);

    indexCtrl.$inject = ['$scope', 'apiService', 'notificationService'];

    function indexCtrl($scope, apiService, notificationService) {
        $scope.pageClass = 'page-home';
        $scope.loadingTests = true;
        $scope.loadingGenres = true;
        $scope.isReadOnly = true;

        $scope.tests = [];
        $scope.loadData = loadData;

        function loadData() {

            //apiService.get('/api/Test/Get/' + 'F75C67AB-487A-469E-841B-43FF2EF65724').then(function (result) {
            //    debugger;
            //}, function () {
            //    debugger;
            //});

            //apiService.getAll('/api/Test/GetAll/').then(function (result) {
            //    $scope.tests = result.data;
            //    $scope.loadingTests = false;
            //}, function () {
            //    debugger;
            //});


            //var testParam = {
            //    Name: "123"
            //}
            //apiService.getByParam('/api/Test/FilterByParam/', testParam).then(function (result) {
            //    debugger;
            //}, function () {
            //    debugger;
            //});

            //apiService.get('/api/Test/GetAll', null,
            //            moviesLoadCompleted,
            //            moviesLoadFailed);

            //apiService.get("/api/genres/", null,
            //    genresLoadCompleted,
            //    genresLoadFailed);
        }

        function genresLoadCompleted(result) {
            var genres = result.data;
            Morris.Bar({
                element: "genres-bar",
                data: genres,
                xkey: "Name",
                ykeys: ["NumberOfMovies"],
                labels: ["Number Of Movies"],
                barRatio: 0.4,
                xLabelAngle: 55,
                hideHover: "auto",
                resize: 'true'
            });
            //.on('click', function (i, row) {
            //    $location.path('/genres/' + row.ID);
            //    $scope.$apply();
            //});

            $scope.loadingGenres = false;
        }

        loadData();

        $scope.remove=function(id) {
            debugger;
            apiService.remove('api/Test/Delete/' + id).then(function (result) {
                debugger;
            }, function () {
                debugger;
            });
        }





         $scope.myInterval = 5000;
  $scope.noWrapSlides = false;
  $scope.active = 0;
  var slides = $scope.slides = [];
  var currIndex = 0;

 

  $scope.randomize = function() {
    var indexes = generateIndexesArray();
    assignNewIndexesToSlides(indexes);
  };

  for (var i = 0; i < 4; i++) {
      var newWidth = 600 + slides.length + 1;
      slides.push({
          image: '../Content/images/' + (i+1) + '.JPG',
          text: ['Nice image', 'Awesome photograph', 'That is so cool', 'I love that'][slides.length % 4],
          id: currIndex++
      });
  }

  // Randomize logic below

  function assignNewIndexesToSlides(indexes) {
    for (var i = 0, l = slides.length; i < l; i++) {
      slides[i].id = indexes.pop();
    }
  }

  function generateIndexesArray() {
    var indexes = [];
    for (var i = 0; i < currIndex; ++i) {
      indexes[i] = i;
    }
    return shuffle(indexes);
  }

  // http://stackoverflow.com/questions/962802#962890
  function shuffle(array) {
    var tmp, current, top = array.length;

    if (top) {
      while (--top) {
        current = Math.floor(Math.random() * (top + 1));
        tmp = array[current];
        array[current] = array[top];
        array[top] = tmp;
      }
    }

    return array;
  }

    }

})(angular.module('calconApp'));
