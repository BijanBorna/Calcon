﻿(function (app) {
    'use strict';

    app.controller('rootCtrl', rootCtrl);

    rootCtrl.$inject = ['$scope', '$location', 'membershipService', '$rootScope', '$window'];

    function rootCtrl($scope, $location, membershipService, $rootScope, $window) {

        $scope.userData = {
            isUserLoggedIn: false,
            displayUserInfo: displayUserInfo
        };
        
        $scope.logout = logout;

        function displayUserInfo() {
            membershipService.isUserLoggedIn()
                .then(function () {
                    $scope.userData.isUserLoggedIn = true;
                    $scope.username = $rootScope.repository.loggedUser.fullname;
                    $scope.loadData();
                    if ($rootScope.previousState)
                        $location.path($rootScope.previousState);
                    else
                        $location.path('/');
                }, function () {
                });
        }

        function logout() {
            $scope.userData.isUserLoggedIn = false;
            membershipService.removeCredentials();
            $location.path('#/');
            $scope.userData.displayUserInfo();
        }

        $scope.userData.displayUserInfo();
    }

})(angular.module('calconApp'));
