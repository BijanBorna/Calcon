﻿(function (app) {
    'use strict';

    app.factory('membershipService', membershipService);

    membershipService.$inject = ['apiService', '$q', 'notificationService', '$http', '$base64', '$cookieStore', '$rootScope'];

    function membershipService(apiService, $q, notificationService, $http, $base64, $cookieStore, $rootScope) {

        var service = {
            login: login,
            register: register,
            saveCredentials: saveCredentials,
            removeCredentials: removeCredentials,
            isUserLoggedIn: isUserLoggedIn
        }

        function login(user) {
            var defferd = $q.defer();
            apiService.getByParam('/api/Account/Login', user).then(function (data) {
                defferd.resolve(data);
            }, function (response) {
                removeCredentials();
                defferd.reject(response);
            });

            return defferd.promise;
        }

        function register(user) {
            var defferd = $q.defer();
            apiService.getByParam('/api/Account/Register', user).then(function (data) {
                defferd.resolve(data);
            }, function (response) {
                removeCredentials();
                defferd.reject(response);
            });

            return defferd.promise;
        }

        function saveCredentials(user) {
            var membershipData = $base64.encode(user.email + ':' + user.password);

            $rootScope.repository = {
                loggedUser: {
                    email: user.email,
                    authdata: membershipData,
                    fullname: user.firstname + ' ' + user.lastname
                }
            };

            $http.defaults.headers.common['Authorization'] = 'Basic ' + membershipData;
            $cookieStore.put('repository', $rootScope.repository);
        }

        function removeCredentials() {
            $rootScope.repository = {};
            $cookieStore.remove('repository');
            $http.defaults.headers.common.Authorization = '';
        };

        function isUserLoggedIn() {
            //return ;
            var defferd = $q.defer();
            if ($rootScope.repository.loggedUser != null) {
                var user = getUserData();
                login(user).then(function () {
                    defferd.resolve();
                }, function () {
                    defferd.reject();
                });
            } else {
                defferd.reject();
            }
            return defferd.promise;
        }

        var getUserData = function () {
            var userDecode = $base64.decode($rootScope.repository.loggedUser.authdata).split(':');
            var user = {
                email: userDecode[0],
                password: userDecode[1]
            }
            return user;
        }

        return service;
    }


})(angular.module('calconApp'));