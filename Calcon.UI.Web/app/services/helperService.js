﻿(function (app) {
    'use strict';

    app.factory('helperService', helperService);

    function helperService() {
        var service = {
            mask: mask,
            unmask: unmask,
            getHierarchicalDataSource: getHierarchicalDataSource,
        };

        function mask(element) {
            element.addClass('relativeClass');
            element.append('<div class=\'loading\'></div>');
        }

        function unmask(element) {
            element.find('.loading').hide();
        }
        function getHierarchicalDataSource(data, idField, parentField) {
            var idToNodeMap = {};
            angular.forEach(data, function (obj) {
                            obj.Children =[];
                        });
            var root = null;
            var childrenIds = [];
            for (var i = 0; i < data.length; i++) {
                var datum = data[i];
                //datum.Children = [];
                var parentNode = {};
                var idToNodeMap = data.reduce(function (map, node) {
                    map[node[idField]] = node;
                    return map;
                }, {});
                idToNodeMap[datum[idField]] = datum;

                if (typeof datum[parentField] === "undefined" || datum[parentField] == null) {
                    root = datum;
                } else {
                    childrenIds.push(datum[idField]);
                    parentNode = idToNodeMap[datum[parentField][idField]];
                    parentNode.Children.push(datum);
                }

            }
            data = data.filter(function (el) {
                return childrenIds.indexOf(el[idField]) < 0;
            });
            return data;
        }

        return service;
    }
})(angular.module('calconApp'));
