﻿(function (app) {
    'use strict';
    app.factory('apiService', apiService);
    apiService.$inject = ['$http', '$q', '$location', 'notificationService', '$rootScope', '$resource'];

    function apiService($http, $q, $location, notificationService, $rootScope, $resource) {
        var service = {
            postHttp: postHttp,
            getHttp: getHttp,
            get: get,
            getAllByParam: getAllByParam,
            getByParam: getByParam,
            getAll: getAll,
            save: save,
            remove: remove,
            getSync: getSync
        };

        function postHttp(url, params) {
            return $http.post(url, params);
        }
        function getHttp(url) {
            return $http.get(url);
        }
        function get(url) {
            var defferd = $q.defer();
            var action = $resource(url, {}, {
                get: {
                    method: 'GET',
                    interceptor: {
                        response: function (response) {
                            return response;
                        }
                    }
                }
            });
            action.get({}, function (result) {
                defferd.resolve(result);
            }, function (response) {
                defferd.reject(response);
            });

            return defferd.promise;
        }
        function getAllByParam(url, param) {
            var defferd = $q.defer();

            var action = $resource(url, param,
                {
                    getAllByParam: {
                        method: 'Post',
                        isArray: true,
                        interceptor: {
                            response: function (response) {
                                return response;
                            }
                        }
                    }
                });

            action.getAllByParam(url, param,
                function (result) {
                    defferd.resolve(result);
                }, function (response) {
                    defferd.reject(response);
                });

            return defferd.promise;
        }
        function getByParam(url, param) {
            var defferd = $q.defer();

            var action = $resource(url, param,
                {
                    getByParam: {
                        method: 'Post',
                        interceptor: {
                            response: function (response) {
                                return response;
                            }
                        }
                    }
                });

            action.getByParam(url, param,
                function (result) {
                    defferd.resolve(result);
                }, function (response) {
                    defferd.reject(response);
                });

            return defferd.promise;
        }
        function getAll(url) {
            var defferd = $q.defer();

            var action = $resource(url, {},
                {
                    getAll: {
                        method: 'GET', params: {},
                        isArray: true,
                        interceptor: {
                            response: function (response) {
                                return response;
                            }
                        }
                    }
                });

            action.getAll(url,
                function (result) {
                    defferd.resolve(result);
                }, function (response) {
                    defferd.reject(response);
                });

            return defferd.promise;
        }
        function save(url, entity) {
            var defferd = $q.defer();

            $resource(url).save(entity,
                function (result) {
                    defferd.resolve(result);
                }, function (response) {
                    defferd.reject(response);
                });

            return defferd.promise;
        }
        function remove(url) {
            var defferd = $q.defer();

            var action = $resource(url, {},
                {
                    remove: { method: 'DELETE' }
                });
            action.remove({},
                function (result) {
                    defferd.resolve(result);
                }, function (response) {
                    defferd.reject(response);
                });

            return defferd.promise;
        }
        function getSync(url) {
            var dataText = $.ajax({
                type: "GET",
                url: url,
                async: false
            }).responseText;
            var data = JSON.parse(dataText);
            return data;
        }

        //function getAll(url, param) {
        //    var defferd = $q.defer();

        //    var action = $resource(url, param,
        //                {
        //                    getAll: { method: 'GET', params: {}, isArray: true }
        //                });

        //    action.getAll(url,
        //        function (result) {
        //            defferd.resolve(result);
        //        }, function (response) {
        //            defferd.reject(response);
        //        });

        //    return defferd.promise;
        //}
        //function remove(url, param) {
        //    var defferd = $q.defer();

        //    var action = $resource(url, {},
        //                {
        //                    remove: { method: 'DELETE', params: {} }
        //                });
        //    action.remove(param,
        //        function (result) {
        //            defferd.resolve(result);
        //        }, function (response) {
        //            defferd.reject(response);
        //        });

        //    return defferd.promise;
        //}
        //function save(url, param) {

        //    var defferd = $q.defer();
        //    $resource(url).save(param, function (result) {
        //        defferd.resolve(result);
        //    }, function (response) {
        //        defferd.reject(response);
        //    });
        //    return defferd.promise;
        //}
        //function get(url,param) {
        //    var defferd = $q.defer();
        //    var action = $resource(url,
        //        param, {
        //            get: {
        //                method: 'GET',
        //                params: param
        //            }
        //        });
        //    action.get(param,
        //        function (result) {
        //            defferd.resolve(result);
        //        }, function (response) {
        //            defferd.reject(response);
        //        });

        //    return defferd.promise;
        //}

        //function update(entity, url) {
        //    var defferd = $q.defer();

        //    var action = $resource(url, {},
        //                {
        //                    update: { method: 'PUT', params: {} },
        //                });
        //    action.update(entity,
        //        function (result) {
        //            defferd.resolve(result);
        //        }, function (response) {
        //            defferd.reject(response);
        //        });

        //    return defferd.promise;
        //}



        return service;
    }

})(angular.module('calconApp'));

(function (app) {
    'use strict';

    app.factory('exceptionInterceptor', exceptionInterceptor)
        .config(config);

    exceptionInterceptor.$inject = ['$q', 'notificationService'];

    function exceptionInterceptor($q, notificationService) {
        var service = {
            request: request,
            responseError: responseError
        };

        function request(request) {
            return request;
        }
        function responseError(response) {
            if (response.statusText && response.status === 400) {
                if (Array.isArray(response.data)) {
                    notificationService.displayError(response.data);
                } else {
                    notificationService.displayError(response.data.Message);
                }
            }
            return $q.reject(response);
        }

        return service;
    }

    config.$inject = ['$httpProvider'];
    function config($httpProvider) {
        $httpProvider.interceptors.push('exceptionInterceptor');
    }

})(angular.module('calconApp'));
