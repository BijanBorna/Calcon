﻿(function () {
    'use strict';

    var core = [
        'ngRoute',
        'ngCookies',
        'base64',
        'angularValidator',
        'angucomplete-alt',
        'ngResource'
    ];

    var ui = [
        'ui.bootstrap',
        'chieffancypants.loadingBar',
        'ui.tree',
        'angular-linq'
    ];

    angular.module('calconApp', core.concat(ui))
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "app/components/home/index.html",
                controller: "indexCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/login", {
                templateUrl: "app/components/account/login.html",
                controller: "loginCtrl"
            })
            .when("/register", {
                templateUrl: "app/components/account/register.html",
                controller: "registerCtrl"
            })
            .when("/company/:id?", {
                templateUrl: "app/components/company/company.html",
                controller: "companyCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/department/:id?", {
                templateUrl: "app/components/department/department.html",
                controller: "departmentCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/employee/:id?", {
                templateUrl: "app/components/employee/employee.html",
                controller: "employeeCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .otherwise({ redirectTo: "/" });
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
    function run($rootScope, $location, $cookieStore, $http) {
        // handle page refreshes
        $rootScope.repository = $cookieStore.get('repository') || {};
        if ($rootScope.repository.loggedUser) {
            $http.defaults.headers.common['Authorization'] = $rootScope.repository.loggedUser.authdata;
        }

        $(document).ready(function () {
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });

            $('.fancybox-media').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                }
            });

            $('[data-toggle=offcanvas]').click(function () {
                $('.row-offcanvas').toggleClass('active');
            });
        });
    }

    isAuthenticated.$inject = ['membershipService', '$rootScope', '$location'];
    function isAuthenticated(membershipService, $rootScope, $location) {
        membershipService.isUserLoggedIn().then(function () { }, function () {
            $rootScope.previousState = $location.path();
            $location.path('/login');
        });
    }
})();