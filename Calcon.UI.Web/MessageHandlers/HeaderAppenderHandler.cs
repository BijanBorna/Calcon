﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Calcon.UI.Web.MessageHandlers
{
    public class HeaderAppenderHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            response.Headers.Add("X-WebAPI-Header", "Web API Unit testing in chsakell's blog.");
            return response;
        }
    }
}