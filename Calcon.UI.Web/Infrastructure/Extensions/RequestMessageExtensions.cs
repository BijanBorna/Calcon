﻿using System.Net.Http;
using System.Web.Http.Dependencies;
using Calcon.Service.Query.Contract;

namespace Calcon.UI.Web.Infrastructure.Extensions
{
    public static class RequestMessageExtensions
    {
        internal static IUserQueryService GetMembershipService(this HttpRequestMessage request)
        {
            return request.GetService<IUserQueryService>();
        }
        private static TService GetService<TService>(this HttpRequestMessage request)
        {
            IDependencyScope dependencyScope = request.GetDependencyScope();
            TService service = (TService)dependencyScope.GetService(typeof(TService));

            return service;
        }
    }
}