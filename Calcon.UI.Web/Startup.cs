﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Calcon.UI.Web.Startup))]

namespace Calcon.UI.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
