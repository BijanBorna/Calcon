﻿using System;
using System.Web.Http;
using Calcon.Service.Command.Contract;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.UI.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DepartmentController : ApiBaseController
    {
        private readonly IDepartmentQueryService _departmentQueryService;
        private readonly IDepartmentCommandService _departmentCommandService;

        public DepartmentController(IDepartmentQueryService departmentQueryService, IDepartmentCommandService departmentCommandService)
        {
            _departmentQueryService = departmentQueryService;
            _departmentCommandService = departmentCommandService;
        }

        [Route("api/Department/Get/{id}")]
        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            return CreateIHttpActionResult(() => _departmentQueryService.Get(id));
        }

        [Route("api/Department/GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            return CreateIHttpActionResult(() => _departmentQueryService.GetAll());
        }

        [Route("api/Department/GetForTree")]
        [HttpPost]
        public IHttpActionResult GetForTree(Guid parentId)
        {
            return CreateIHttpActionResult(() => _departmentQueryService.GetForTree(parentId));
        }

        [Route("api/Department/GetForCombo")]
        [HttpGet]
        public IHttpActionResult GetForCombo()
        {
            return CreateIHttpActionResult(() => _departmentQueryService.GetForCombo());
        }

        [Route("api/Department/GetForCombo/{parentId}")]
        [HttpGet]
        public IHttpActionResult GetForCombo(Guid parentId)
        {
            return CreateIHttpActionResult(() => _departmentQueryService.GetForCombo(parentId));
        }

        [Route("api/Department/Insert")]
        [HttpPost]
        public IHttpActionResult Insert(DepartmentViewModel department)
        {
            return CreateIHttpActionResult(() =>
            {
                department.Id= Guid.NewGuid();
                _departmentCommandService.Insert(department);
                return department;
            });
        }
        
        [Route("api/Department/Update")]
        [HttpPost]
        public IHttpActionResult Update(DepartmentViewModel department)
        {
            return CreateIHttpActionResult(() =>
            {
                _departmentCommandService.Update(department);
                return department;
            });
        }

        [Route("api/Department/Delete/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            return CreateIHttpActionResult(() => _departmentCommandService.Delete(id));
        }
    }
}
