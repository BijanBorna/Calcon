﻿using System.Web.Http;
using Calcon.Service.Command.Contract;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.ViewModels.UserViewModel;

namespace Calcon.UI.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AccountController : ApiBaseController
    {
        private readonly IUserQueryService _userQueryService;
        private readonly IUserCommandService _userCommandService;

        public AccountController(IUserQueryService userQueryService, IUserCommandService userCommandService)
        {
            _userQueryService = userQueryService;
            _userCommandService = userCommandService;
        }

        [AllowAnonymous]
        [Route("api/Account/Login")]
        [HttpPost]
        public IHttpActionResult Login(UserViewModel userViewModel)
        {
            return CreateIHttpActionResult(() =>
            {
                var res = _userQueryService.ValidateUser(userViewModel.Email, userViewModel.Password);
                return new UserViewModel
                {
                    FirstName = res.User.FirstName,
                    LastName = res.User.LastName,
                    Email = res.User.Email,
                };
            });
        }

        [AllowAnonymous]
        [Route("api/Account/Register")]
        [HttpPost]
        public IHttpActionResult Register(UserViewModel user)
        {
            return CreateIHttpActionResult(() =>
            {
                _userCommandService.Create(user);
                return new UserViewModel
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                };
            });
        }
    }
}
