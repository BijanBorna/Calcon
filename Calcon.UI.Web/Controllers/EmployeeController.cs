﻿using System;
using System.Web.Http;
using Calcon.Service.Command.Contract;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.UI.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EmployeeController : ApiBaseController
    {
        private readonly IEmployeeQueryService _employeeQueryService;
        private readonly IEmployeeCommandService _employeeCommandService;


        public EmployeeController(IEmployeeQueryService employeeQueryService, IEmployeeCommandService employeeCommandService)
        {
            _employeeQueryService = employeeQueryService;
            _employeeCommandService = employeeCommandService;
        }

        [Route("api/Employee/Get/{id}")]
        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            return CreateIHttpActionResult(() => _employeeQueryService.Get(id));
        }

        [Route("api/Employee/GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            return CreateIHttpActionResult(() => _employeeQueryService.GetAll());
        }

        [Route("api/Employee/GetForTree")]
        [HttpPost]
        public IHttpActionResult GetForTree(Guid parentId)
        {
            return CreateIHttpActionResult(() => _employeeQueryService.GetForTree(parentId));
        }

        [Route("api/Employee/Insert")]
        [HttpPost]
        public IHttpActionResult Insert(EmployeeViewModel employee)
        {
            return CreateIHttpActionResult(() =>
            {
                employee.Id= Guid.NewGuid();
                _employeeCommandService.Insert(employee);
                return employee;
            });
        }
        
        [Route("api/Employee/Update")]
        [HttpPost]
        public IHttpActionResult Update(EmployeeViewModel employee)
        {
            return CreateIHttpActionResult(() =>
            {
                _employeeCommandService.Update(employee);
                return employee;
            });
        }

        [Route("api/Employee/Delete/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            return CreateIHttpActionResult(() => _employeeCommandService.Delete(id));
        }
    }
}
