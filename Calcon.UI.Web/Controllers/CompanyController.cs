﻿using System;
using System.Web.Http;
using Calcon.Service.Command.Contract;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.UI.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CompanyController : ApiBaseController
    {
        private readonly ICompanyQueryService _companyQueryService;
        private readonly ICompanyCommandService _companyCommandService;


        public CompanyController(ICompanyQueryService companyQueryService, ICompanyCommandService companyCommandService)
        {
            _companyQueryService = companyQueryService;
            _companyCommandService = companyCommandService;
        }

        [Route("api/Company/Get/{id}")]
        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            return CreateIHttpActionResult(() => _companyQueryService.Get(id));
        }

        [Route("api/Company/GetAll")]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            return CreateIHttpActionResult(() => _companyQueryService.GetAll());
        }

        [Route("api/Company/GetForTree")]
        [HttpGet]
        public IHttpActionResult GetForTree()
        {
            return CreateIHttpActionResult(() => _companyQueryService.GetForTree());
        }

        [Route("api/Company/GetForCombo")]
        [HttpGet]
        public IHttpActionResult GetForCombo()
        {
            return CreateIHttpActionResult(() => _companyQueryService.GetForCombo());
        }

        [Route("api/Company/Insert")]
        [HttpPost]
        public IHttpActionResult Insert(CompanyViewModel company)
        {
            return CreateIHttpActionResult(() =>
            {
                company.Id= Guid.NewGuid();
                _companyCommandService.Insert(company);
                return company;
            });
        }
        
        [Route("api/Company/Update")]
        [HttpPost]
        public IHttpActionResult Update(CompanyViewModel company)
        {
            return CreateIHttpActionResult(() =>
            {
                _companyCommandService.Update(company);
                return company;
            });
        }

        [Route("api/Company/Delete/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            return CreateIHttpActionResult(() => _companyCommandService.Delete(id));
        }
    }
}
