﻿using System.Web.Mvc;

namespace Calcon.UI.Web.Controllers
{
    public class HomeController : Controller
    {
       public ActionResult Index()
        {
            return View();
        }
    }
}
