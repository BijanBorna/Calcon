﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using Calcon.Common.Aspects.Validation;

//using FluentValidation;
//using Calcon.Common.ExceptionHelper;
//using Calcon.Common.ReflectionHelper;

namespace Calcon.UI.Web.Controllers
{
    //[CustomExceptionFilter]
    public class ApiBaseController : ApiController
    {
        //protected readonly IBaseEntityService<Error> _errorsService;
        //protected readonly IUnitOfWork _unitOfWork;

        //public ApiBaseController(IBaseEntityService<Error> errorsService, IUnitOfWork unitOfWork)
        //{
        //    _errorsService = errorsService;
        //    _unitOfWork = unitOfWork;
        //}

        #region [IHttpActionResult]
        private IHttpActionResult CreateBaseIHttpActionResult(Func<IHttpActionResult> func)
        {
            IHttpActionResult response = Ok();
            try
            {
                //TransactionHelper.Execute(() =>
                //{
                response = func.Invoke();
                //});
            }
            catch (ValidationException ex)
            {
                LogError(ex);
                response = Content(HttpStatusCode.BadRequest, ex.Errors.Select(s => s.Message).ToList());
            }
            catch (Exception ex)
            {
                LogError(ex);
                //if (ex.GetType() == typeof(ValidationException))
                //{
                //    var validationException = ex as ValidationException;
                //    response = Content(HttpStatusCode.BadRequest, validationException.Errors.Select(s => s.ErrorMessage).ToList());
                //}
                //else
                //{
                    response = Content(HttpStatusCode.BadRequest, ex);

                //}
            }
            return response;
        }
        protected IHttpActionResult CreateIHttpActionResult(Func<object> func) => CreateBaseIHttpActionResult(() => Ok(func.Invoke()));

        protected IHttpActionResult CreateIHttpActionResult(Action action) => CreateBaseIHttpActionResult(() =>
        {
            action.Invoke();
            return Ok();
        });

        #endregion

        #region [HttpResponseMessage]
        private HttpResponseMessage CreateBaseHttpResponse(Func<HttpResponseMessage> func)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            try
            {
                //TransactionHelper.Execute(() =>
                //{
                response = func.Invoke();
                //});
            }
            catch (Exception ex)
            {
                LogError(ex);
                response = Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
            return response;
        }
        protected HttpResponseMessage CreateHttpResponse(Func<object> func) => CreateBaseHttpResponse(() => Request.CreateResponse(HttpStatusCode.OK, func.Invoke()));
        protected HttpResponseMessage CreateHttpResponse(Action action) => CreateBaseHttpResponse(() =>
        {
            action.Invoke();
            return Request.CreateResponse(HttpStatusCode.OK);
        });

        #endregion

        #region [LogError]
        private void LogError(Exception ex)
        {
            try
            {
                //Error _error = new Error()
                //{
                //    Message = ex.Message,
                //    StackTrace = ex.StackTrace
                //};

                //_errorsService.Add(_error);
                //_unitOfWork.Commit();
            }
            catch { }
        }
        #endregion

    }
    #region [CustomExceptionFilter]
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;
            String message = String.Empty;
            var exceptionType = actionExecutedContext.Exception.GetType();
            //if (exceptionType == typeof(ValidationException))
            //{
            //    actionExecutedContext.Exception=(actionExecutedContext.Exception as ValidationException);
            //    status=HttpStatusCode.BadRequest;
            //    base.OnException(actionExecutedContext);
            //}
            //else 
            if (exceptionType == typeof(UnauthorizedAccessException))
            {
                message = "Access to the Web API is not authorized.";
                status = HttpStatusCode.Unauthorized;
            }
            else if (exceptionType == typeof(DivideByZeroException))
            {
                message = "Internal Server Error.";
                status = HttpStatusCode.InternalServerError;
            }
            else
            {
                message = "Not found.";
                status = HttpStatusCode.NotFound;
            }
            //actionExecutedContext.Response = new HttpResponseMessage()
            //{
            //    Content = new StringContent(message, System.Text.Encoding.UTF8, "text/plain"),
            //    StatusCode = status
            //};
            base.OnException(actionExecutedContext);
        }
    }
    #endregion
}

