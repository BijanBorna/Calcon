﻿using System;
using Calcon.Component.Command.Contract.Company;
using Calcon.DomainClasses.Domain;
using Calcon.DomainClasses.ValueType;
using Calcon.Framework.Application.Components;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Models;
using Calcon.ModelCalsses.ValueType;

namespace Calcon.Component.Command.CompanyComponent
{
    public class CompanyCommand :
          CommandApplicationComponent<Company, CompanyModel>
        , ICompanyCommand
    {
        public CompanyCommand(IRepository repository) : base(repository)
        {
        }

        #region Mapping

        protected override void MapCreate(CompanyModel model)
        {
            MapEntity(model);
        }
        protected override void MapEdit(CompanyModel model)
        {
            MapEntity(model);
        }

        protected void MapEntity(CompanyModel model)
        {
            Entity.Name = model.Name;
            Entity.Tel = model.Tel;
            Entity.Fax = model.Fax;
            Entity.WebSite = model.WebSite;
            Entity.Email = model.Email;
            Entity.Address = MapToAddress(model.Address);
            //if (model.Departments != null)
            //    Entity.Departments = model.Departments.Select(MapToDepartment).ToList();
        }

        //protected Department MapToDepartment(DepartmentModel model)
        //{
        //    if (model == null) return null;

        //    return new Department
        //    {
        //        Id = model.Id,
        //        Name = model.Name
        //    };
        //}
        protected Address MapToAddress(AddressModel model)
        {
            if (model == null || model.Id == Guid.Empty)
                return null;

            if (Entity.Address == null || Entity.Address.Id != model.Id)
                return new Address
                {
                    Id = model.Id,
                    Place = model.Place
                };

            Repository.Delete<Address>(Entity.Address.Id);
            return new Address
            {
                Id = model.Id,
                Place = model.Place
            };
        }
        #endregion Mapping
    }
}
