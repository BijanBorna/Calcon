﻿using Calcon.Component.Command.Contract.User;
using Calcon.DomainClasses.Domain.User;
using Calcon.Framework.Application.Components;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Models.UserModel;

namespace Calcon.Component.Command.UserComponent
{
    public class UserCommand :
          CommandApplicationComponent<User, UserModel>
        , IUserCommand
    {
        public UserCommand(IRepository repository) : base(repository)
        {
        }

        #region Mapping
        protected override void MapCreate(UserModel model)
        {
            MapEntity(model);
        }
        protected override void MapEdit(UserModel model)
        {
            MapEntity(model);
        }

        protected void MapEntity(UserModel model)
        {
            Entity.FirstName = model.FirstName;
            Entity.LastName = model.LastName;
            Entity.Email = model.Email;
            Entity.HashedPassword = model.HashedPassword;
            Entity.Salt = model.Salt;
        }
        #endregion Mapping
    }
}
