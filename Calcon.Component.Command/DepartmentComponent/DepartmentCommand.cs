﻿using Calcon.Component.Command.Contract.Department;
using Calcon.DomainClasses.Domain;
using Calcon.Framework.Application.Components;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Models;

namespace Calcon.Component.Command.DepartmentComponent
{
    public class DepartmentCommand :
        CommandApplicationComponent<Department, DepartmentModel>
        , IDepartmentCommand
    {
        public DepartmentCommand(IRepository repository) : base(repository)
        {
        }

        #region Mapping
        protected override void MapCreate(DepartmentModel model)
        {
            MapEntity(model);
        }
        protected override void MapEdit(DepartmentModel model)
        {
            MapEntity(model);
        }

        protected void MapEntity(DepartmentModel model)
        {
            Entity.Name = model.Name;
            Entity.Company = MapToCompany(model.Company);
        }
        protected Company MapToCompany(CompanyModel model)
        {
            if (model == null) return null;

            return Repository.Get<Company>(model.Id);
        }
        #endregion Mapping
    }
}
