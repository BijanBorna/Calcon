﻿using Autofac;
using Calcon.Component.Command.CompanyComponent;
using Calcon.Framework.Config;

namespace Calcon.Component.Command.Config
{
    public class AutofacBusinessCommandConfig : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            // Business
            builder.RegisterAssemblyModules(typeof(AutofacFrameworkConfig).Assembly);

            builder.RegisterAssemblyTypes(typeof(CompanyCommand).Assembly)
               .Where(t => t.Name.EndsWith("Command"))
               .AsImplementedInterfaces().InstancePerRequest();

        }
    }
}
