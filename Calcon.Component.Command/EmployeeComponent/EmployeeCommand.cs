﻿using System;
using Calcon.Component.Command.Contract.Employee;
using Calcon.DomainClasses.Domain;
using Calcon.DomainClasses.Enums;
using Calcon.DomainClasses.ValueType;
using Calcon.Framework.Application.Components;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Models;
using Calcon.ModelCalsses.ValueType;

namespace Calcon.Component.Command.EmployeeComponent
{
    public class EmployeeCommand :
          CommandApplicationComponent<Employee, EmployeeModel>
        , IEmployeeCommand
    {
        public EmployeeCommand(IRepository repository) : base(repository)
        {
        }

        #region Mapping
        protected override void MapCreate(EmployeeModel model)
        {
            MapEntity(model);
        }
        protected override void MapEdit(EmployeeModel model)
        {
            MapEntity(model);
        }

        protected void MapEntity(EmployeeModel model)
        {
            Entity.FirstName = model.FirstName;
            Entity.LastName = model.LastName;
            Entity.Gender = (Gender)model.Gender;
            Entity.Birthdate = model.Birthdate;
            Entity.Tel = model.Tel;
            Entity.Mobile = model.Mobile;
            Entity.Email = model.Email;
            Entity.Address = MapToAddress(model.Address);

            Entity.Department = MapToDepartment(model.Department);
        }
        protected Department MapToDepartment(DepartmentModel model)
        {
            if (model == null) return null;

            return Repository.Get<Department>(model.Id);
        }
        protected Address MapToAddress(AddressModel model)
        {
            if (model == null || model.Id == Guid.Empty)
                return null;

            if (Entity.Address == null || Entity.Address.Id != model.Id)
                return new Address
                {
                    Id = model.Id,
                    Place = model.Place
                };

            Repository.Delete<Address>(Entity.Address.Id);
            return new Address
            {
                Id = model.Id,
                Place = model.Place
            };
        }
        #endregion Mapping
    }
}
