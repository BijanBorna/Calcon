﻿using Calcon.DomainClasses.Domain;
using Calcon.Framework.Data.NHSessionManager;
using FluentNHibernate;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate;
using NHibernate.Cfg;
using NUnit.Framework;

namespace Calcon.UnitTests
{
    public class FixtureBase
    {
        protected SessionSource SessionSource { get; set; }
        protected ISession Session { get; private set; }

        public static FluentConfiguration GetConfig()
        {
            return
                Fluently.Configure().Database(
                         //SQLiteConfiguration.Standard.InMemory()
                         MsSqlConfiguration
                        .MsSql2012
                        .ConnectionString(x => x.FromConnectionStringWithKey("DbConnectionStringTest"))
                        //.ShowSql()
                        .AdoNetBatchSize(500)
                    )
                    .Mappings(
                    m => m.AutoMappings.Add(
                         new AutoPersistenceModel()
                             .Conventions.Add(
                                  PrimaryKey.Name.Is(x => "Id"),
                                  ConventionBuilder.Id.Always(c => c.GeneratedBy.GuidNative()),
                                  DefaultLazy.Never(),
                                  ForeignKey.EndsWith("Ref"),
                                  Table.Is(t => "tbl" + t.EntityType.Name),
                                  DefaultCascade.SaveUpdate(),
                                  AutoImport.Never()
                             )
                             .AddEntityAssembly(typeof(Company).Assembly))
                             .ExportTo(System.Environment.CurrentDirectory));
        }

        [SetUp]
        public void SetupContext()
        {
            Configuration cfg = GetConfig().BuildConfiguration();
            VeConfig.BuildIntegratedFluentlyConfiguredEngine(ref cfg);

            SessionSource = new SessionSource(GetConfig());

            Session = SessionSource.CreateSession();
            SessionSource.BuildSchema(Session, false);
        }

        [TearDown]
        public void TearDownContext()
        {
            Session.Close();
            Session.Dispose();
        }
    }
}
