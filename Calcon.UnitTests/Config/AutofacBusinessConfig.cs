﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using OnlineStore.Business.CommandBusiness;
using OnlineStore.Business.QueryBusiness;

namespace OnlineStore.Business.AutofacConfig
{
    public class AutofacBusinessConfig : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            // Business
            builder.RegisterAssemblyModules(typeof(Data.AutofacConfig.AutofacDataConfig).Assembly);
            builder.RegisterAssemblyModules(typeof(Validation.AutofacConfig.AutofacValidationConfig).Assembly);
            builder.RegisterAssemblyModules(typeof(Core.AutofacConfig.AutofacCoreConfig).Assembly);

            builder.RegisterAssemblyTypes(typeof(TestQueryBusiness).Assembly)
               .Where(t => t.Name.EndsWith("QueryBusiness"))
               .AsImplementedInterfaces().InstancePerRequest();
            builder.RegisterAssemblyTypes(typeof(TestCommandBusiness).Assembly)
              .Where(t => t.Name.EndsWith("CommandBusiness"))
              .AsImplementedInterfaces().InstancePerRequest();

        }
    }
}
