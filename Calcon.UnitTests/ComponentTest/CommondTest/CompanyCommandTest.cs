﻿using System;
using Calcon.Component.Command.CompanyComponent;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Models;
using NUnit.Framework;

namespace Calcon.UnitTests.ComponentTest.CommondTest
{
    [TestFixture]
    class CompanyCommandTest : FixtureBase
    {
        [Test]
        public void ShouldAddNewCompany()
        {
            var model = new CompanyModel
            {
                Id = Guid.Parse("A5244132-BA06-4160-AB8E-F86B1739126B"),
                Name = "o",
                Tel = "123456",
                Fax = "654321",
                WebSite = "a.com",
                Email = "asd@a.com"
            };
            var repository = new Repository(Session);
            var company = new CompanyCommand(repository);
            Assert.AreEqual(true, company.Create(model));
            company.CommitChange();
        }

        [Test]
        public void ShoulUpdateCompany()
        {
            ShouldAddNewCompany();
            var model = new CompanyModel
            {
                Id = Guid.Parse("A5244132-BA06-4160-AB8E-F86B1739126B"),
                Name = "Test11",
                Tel = "123456000",
                Fax = "654321000",
                WebSite = "abc.com",
                Email = "asd@abc.com"
            };
            var repository = new Repository(Session);
            var company = new CompanyCommand(repository);
            Assert.AreEqual(true, company.Edit(model));
            company.CommitChange();
        }

        [Test]
        public void ShoulRemoveCompany()
        {
            ShouldAddNewCompany();
            var repository = new Repository(Session);
            var company = new CompanyCommand(repository);
            Assert.AreEqual(true, company.Delete(Guid.Parse("A5244132-BA06-4160-AB8E-F86B1739126B")));
            company.CommitChange();
        }
    }
}
