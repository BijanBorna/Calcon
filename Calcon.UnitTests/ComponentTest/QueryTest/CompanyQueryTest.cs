﻿using System;
using System.Linq;
using Calcon.Component.Command.CompanyComponent;
using Calcon.Component.Query.CompanyComponent;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Models;
using NUnit.Framework;

namespace Calcon.UnitTests.ComponentTest.QueryTest
{
    [TestFixture]
    class CompanyQueryTest : FixtureBase
    {
        [Test]
        public void ShouldGetCompany()
        {
            var repository = new Repository(Session);
            var company = new CompanyQuery(repository);
            Assert.AreEqual(true, company.GetById(Guid.Parse("A5244132-BA06-4160-AB8E-F86B1739126B")));
        }

        [Test]
        public void ShouldGetAllCompany()
        {
            var repository = new Repository(Session);
            var company = new CompanyQuery(repository);
            Assert.AreEqual(0, company.GetAll().Count());
        }
    }
}
