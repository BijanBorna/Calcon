﻿using Calcon.DomainClasses.Domain;
using FluentNHibernate.Testing;
using NUnit.Framework;

namespace Calcon.UnitTests.EntityTest
{
    [TestFixture]
    class DepartmentMappingFixture : FixtureBase
    {
        [Test]
        public void Can_correctly_map_Department()
        {
            new PersistenceSpecification<Department>(Session)
                    .CheckProperty(c => c.Name, "Calcon")
                    .VerifyTheMappings();
        }
    }
}
