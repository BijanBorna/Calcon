﻿using System.Collections;
using Calcon.Framework.Core.Application.Models;

namespace Calcon.UnitTests.EntityTest
{
    public class EntityEqualityComparer : IEqualityComparer
    {
        public new bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (x == null || y == null) return false;

            if (x is IBaseEntity && y is IBaseEntity)
                return ((IBaseEntity) x).Id == ((IBaseEntity) y).Id;

            return x.Equals(y);
        }

        public int GetHashCode(object obj)
        {
            return obj.GetHashCode();
        }
    }
}
