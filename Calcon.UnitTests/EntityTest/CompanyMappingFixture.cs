﻿using System;
using Calcon.DomainClasses.Domain;
using FluentNHibernate.Testing;
using NUnit.Framework;

namespace Calcon.UnitTests.EntityTest
{
    [TestFixture]
    class CompanyMappingFixture : FixtureBase
    {
        [Test]
        public void Can_correctly_map_Company()
        {
            new PersistenceSpecification<Company>(Session)
                    .CheckProperty(c => c.Name, "Calcon")
                    .CheckProperty(c => c.WebSite, "www.calcon.de")
                    //.CheckProperty(c => c.Address, "Addr1")
                    .CheckProperty(c => c.Email, "F.Kriesten@calcon.de")
                    .CheckProperty(c => c.Fax, "1234")
                    .CheckProperty(c => c.Tel, "56789")
                    .VerifyTheMappings();
        }
    }
}
