﻿using System;
using Calcon.DomainClasses.Domain;
using Calcon.DomainClasses.Enums;
using FluentNHibernate.Testing;
using NUnit.Framework;

namespace Calcon.UnitTests.EntityTest
{
    [TestFixture]
    class EmployeeMappingFixture : FixtureBase
    {
        [Test]
        public void Can_correctly_map_Employee()
        {
            new PersistenceSpecification<Employee>(Session)
                    .CheckProperty(c => c.FirstName, "Bijan")
                    .CheckProperty(c => c.LastName, "Bornadel")
                    .CheckProperty(c => c.Gender, Gender.Male)
                    .CheckProperty(c => c.Birthdate, DateTime.Now.AddMonths(3).AddYears(-33))
                    //.CheckProperty(c => c.Address, "Addr1")
                    .CheckProperty(c => c.Email, "3ijan.sm@gmail.com")
                    .CheckProperty(c => c.Tel, "+98 21 22948497")
                    .CheckProperty(c => c.Mobile, "+98 935 5755577")
                    .VerifyTheMappings();
        }
    }
}
