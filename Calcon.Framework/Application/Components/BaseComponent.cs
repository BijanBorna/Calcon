﻿using System;
using Calcon.Framework.Core.Application.Components;
using Calcon.Framework.Core.Application.Models;

namespace Calcon.Framework.Application.Components
{
    public abstract class BaseComponent : IBaseComponent, IDisposable
    {
        public virtual void Dispose()
        {
            //Dispose();
        }
    }
}
