﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Calcon.Framework.Core.Adapter;
using Calcon.Framework.Core.Application.Components;
using Calcon.Framework.Core.Application.Models;
using Calcon.Framework.Data.NHRepository;
using NHibernate.Linq;

namespace Calcon.Framework.Application.Components
{
    public abstract class QueryApplicationComponent<TEntity, TModel> : 
        BaseComponent, 
        IQueryApplicationComponent<TModel>
        where TEntity : class, IBaseEntity, new()
        where TModel : class, IParam, new()
    {
        #region Constructor
        protected QueryApplicationComponent(IRepository repository)
        {
            Repository = repository;
        }
        #endregion

        #region Properties
        protected IRepository Repository { get; }
        #endregion

        protected virtual TModel MapModel(TEntity source)
        {
            return null;
        }
        public TModel GetById(Guid id)
        {
            var entity = Repository.Get<TEntity>(id);
            return MapModel(entity);
        }

        public bool Exists(Guid id)
        {
            return Repository.Query<TEntity>().Any();
        }

        public Task<bool> ExistsAsync(Guid id)
        {
            return Repository.Query<TEntity>().AnyAsync();
        }

        protected IQueryable<TEntity> Query()
        {
            return Repository.Query<TEntity>();
        }
    }
}
