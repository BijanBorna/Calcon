﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Calcon.Common.Aspects.Transaction;
using Calcon.Common.Aspects.Validation;
using Calcon.Framework.Core.Adapter;
using Calcon.Framework.Core.Application.Components;
using Calcon.Framework.Core.Application.Models;
using Calcon.Framework.Core.Domain.Entities;
using Calcon.Framework.Data.NHRepository;
using Calcon.Framework.Data.NHSessionManager;
using NHibernate.Exceptions;

namespace Calcon.Framework.Application.Components
{
    public abstract class CommandApplicationComponent<TEntity, TParam> :
            BaseComponent
            , ICommandApplicationComponent<TParam>
        where TEntity : class, IBaseEntity, new()
        where TParam : class, IParam
    {
        #region Constructor

        protected CommandApplicationComponent(IRepository repository)
        {
            Repository = repository;
        }

        #endregion

        #region Properties

        protected IRepository Repository { get; }
        protected TEntity Entity { get; set; }

        #endregion

        #region Check Validation

        private void CheckValidation()
        {
            var ve2 = VeConfig.GetFluentlyConfiguredEngine();

            var invalidValues = ve2.Validate(Entity);

            if (invalidValues.Any())
            {
                var validation = new ValidationException();
                invalidValues.ToList().ForEach(s => validation.AddError(s.PropertyName, s.Message));
                throw validation;
            }
        }

        #endregion

        #region Create

        protected void NewEntity(Guid id)
        {
            if (Entity == null)
                Entity = new TEntity
                {
                    Id = id,
                    State = EntityChangeState.New
                };
        }

        [Transactional]
        public virtual bool Create(TParam model)
        {
            try
            {
                if (model.Id == Guid.Empty) model.Id = Guid.NewGuid();
                NewEntity(model.Id);
                MapCreate(model);

                CheckValidation();

                Entity = Repository.Save(Entity);
                return true;
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ValidationException(ex.Message);
            }
        }

        [Transactional]
        public virtual Task CreateAsync(TParam model)
        {
            try
            {
                if (model.Id == Guid.Empty) model.Id = Guid.NewGuid();
                NewEntity(model.Id);
                MapCreate(model);

                CheckValidation();

                return Repository.SaveAsync(Entity);
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ValidationException(ex.Message);
            }
        }

        #endregion

        #region Edit

        protected void LoadEntity(Guid id)
        {
            Entity = Repository.Get<TEntity>(id);
            if (Entity != null)
            {
                Entity.ModificationDate = DateTime.Now;
                Entity.State = EntityChangeState.Modified;
            }
        }

        [Transactional]
        public virtual bool Edit(TParam model)
        {
            try
            {
                LoadEntity(model.Id);
                MapEdit(model);

                CheckValidation();

                Repository.Update(Entity);
                return true;
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ValidationException(ex.Message);
            }
        } 

        [Transactional]
        public virtual Task EditAsync(TParam model)
        {
            try
            {
                LoadEntity(model.Id);
                MapEdit(model);

                CheckValidation();

                return Repository.UpdateAsync(Entity);
            }
            catch (ValidationException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new ValidationException(ex.Message);
            }
        } 

        #endregion

        #region Delete

        [Transactional]
        public virtual bool Delete(Guid id)
        {
            try
            {
                Repository.Delete<TEntity>(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new ValidationException("This entity has a child or children.");
            }
        } 

        [Transactional]
        public virtual Task DeleteAsync(Guid id)
        {
            try
            {
                return Repository.DeleteAsync<TEntity>(id);
            }
            catch (Exception ex)
            {
                throw new ValidationException(ex.Message);
            }
        } 

        #endregion

        public virtual void CommitChange()
        {
            try
            {
                Repository.Commit();
            }
            catch (GenericADOException ex)
            {
                throw new ValidationException("Data has a children");
            }
            catch (Exception ex)
            {
                var t = ex.GetType();
                throw new ValidationException(ex.Message);
            }
        }
        public virtual Task CommitChangeAsync()
        {
            return Repository.CommitAsync();
        }

        public override void Dispose()
        {
            Repository.Dispose();
            base.Dispose();
        }

        protected virtual void MapCreate(TParam model)
        {

        }
        protected virtual void MapEdit(TParam model)
        {

        }
    }
}
