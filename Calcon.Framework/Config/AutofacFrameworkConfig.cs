﻿using Autofac;
using Calcon.Framework.Application.Components;
using Calcon.Framework.Data.NHRepository;

namespace Calcon.Framework.Config
{
    public class AutofacFrameworkConfig : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(typeof(CommandApplicationComponent<,>).Assembly)
               .Where(t => t.Name.EndsWith("Component"))
               .AsImplementedInterfaces().InstancePerRequest();

            builder.RegisterType<Repository>().As<IRepository>().InstancePerRequest();

        }
    }
}
