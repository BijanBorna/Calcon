﻿using System.Linq;
using System.Reflection;
using Calcon.Validation.Validation;
using NHibernate.Validator.Cfg.Loquacious;
using NHibernate.Validator.Engine;
using NHibernate.Validator.Cfg;
using NHibernate.Cfg;

namespace Calcon.Framework.Data.NHSessionManager
{
    public class VeConfig
    {
        public static ValidatorEngine GetFluentlyConfiguredEngine()
        {
            var vtor = new ValidatorEngine();
            var configuration = new FluentConfiguration();
            configuration
                    .Register(
                        Assembly
                          .GetAssembly(typeof(CompanyValidation))
                          .GetTypes()
                          .ValidationDefinitions()
                     )
                    .SetDefaultValidatorMode(ValidatorMode.UseExternal);
            vtor.Configure(configuration);
            return vtor;
        }

        /// <summary>
        /// Helping for starting sassion factory
        /// </summary>
        /// <param name="nhConfiguration"></param>
        public static void BuildIntegratedFluentlyConfiguredEngine(ref Configuration nhConfiguration)
        {
            var vtor = new ValidatorEngine();
            var configuration = new FluentConfiguration();
            configuration
                    .Register(
                        Assembly
                          .GetAssembly(typeof(CompanyValidation))
                          .GetTypes()
                          .ValidationDefinitions()
                     )
                    .SetDefaultValidatorMode(ValidatorMode.UseExternal)
                    .IntegrateWithNHibernate
                    .ApplyingDDLConstraints()
                    .And
                    .RegisteringListeners();
            vtor.Configure(configuration);

            //Registering of Listeners and DDL-applying here
            ValidatorInitializer.Initialize(nhConfiguration, vtor);            
        }
    }
}
