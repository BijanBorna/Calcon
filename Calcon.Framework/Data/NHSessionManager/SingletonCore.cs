﻿using NHibernate;
using NHibernate.Cfg;

namespace Calcon.Framework.Data.NHSessionManager
{
    /// <summary>
    /// lazy, lock-free, thread-safe singleton
    /// </summary>
    public class SingletonCore
    {
        private readonly ISessionFactory _sessionFactory;

        SingletonCore()
        {
            Configuration cfg = DbConfig.GetConfig().BuildConfiguration();
            VeConfig.BuildIntegratedFluentlyConfiguredEngine(ref cfg);
            _sessionFactory = cfg.BuildSessionFactory();            
        }

        public static SingletonCore Instance => Nested.Instance;

        public static ISession GetCurrentSession()
        {
            return Instance._sessionFactory.GetCurrentSession();
        }

        public static ISessionFactory SessionFactory => Instance._sessionFactory;

        class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly SingletonCore Instance = new SingletonCore();
        }
    }
}
