﻿using Calcon.DomainClasses.Domain;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Cfg;

namespace Calcon.Framework.Data.NHSessionManager
{
    public class DbConfig
    {
        public static FluentConfiguration GetConfig()
        {
            return
                Fluently.Configure().Database(
                         //SQLiteConfiguration.Standard.InMemory()
                         MsSqlConfiguration
                        .MsSql2012
                        .ConnectionString(x => x.FromConnectionStringWithKey("DbConnectionString"))
                        //.ShowSql()
                        .AdoNetBatchSize(500)
                    )
                    .Mappings(
                    m => m.AutoMappings.Add(
                         new AutoPersistenceModel()
                             .Conventions.Add(
                                  PrimaryKey.Name.Is(x => "Id"),
                                  ConventionBuilder.Id.Always(c => c.GeneratedBy.GuidNative()),
                                  DefaultLazy.Never(),
                                  ForeignKey.EndsWith("Ref"),
                                  Table.Is(t => "tbl" + t.EntityType.Name),
                                  DefaultCascade.None(),
                                  AutoImport.Never()
                             )
                             .AddEntityAssembly(typeof(Company).Assembly))
                             .ExportTo(System.Environment.CurrentDirectory));
        }

        public static void CreateDb()
        {
            bool script = false;         // Show report on consol
            bool export = false;         // Execute on database
            bool dropTables = false;     // Drop table

            new SchemaExport(GetConfig().BuildConfiguration()).ExecuteAsync(script, export, dropTables);
        }

        public static void CreateValidDb()
        {
            bool script = false;         // Show report on consol
            bool export = false;         // Execute on database
            bool dropTables = false;     // Drop table

            Configuration cfg = GetConfig().BuildConfiguration();
            VeConfig.BuildIntegratedFluentlyConfiguredEngine(ref cfg);

            // Start with validation config
            new SchemaExport(cfg).ExecuteAsync(script, export, dropTables);
        }
    }
}
