﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Calcon.Framework.Core.Application.Models;
using NHibernate;
using Calcon.Framework.Data.NHSessionManager;
using NHibernate.Linq;

namespace Calcon.Framework.Data.NHRepository
{
    public class Repository : IRepository, IDisposable
    {
        #region Fields

        private bool _disposed;
        private readonly ISession _session;

        #endregion Fields

        #region Constructors

        public Repository()
        {
            _session = SingletonCore.SessionFactory.OpenSession();
            BeginTransaction();
        }
        public Repository(ISession session)
        {
            _session = session;//.OpenSession();
            BeginTransaction();
        }

        ~Repository()
        {
            Dispose(false);
        }

        #endregion Constructors

        #region Properties

        private bool IsSafeSession => _session != null && _session.IsOpen;

        #endregion Properties

        #region Methods

        // Public Methods 

        public void Dispose()
        {
            Dispose(true);
            // tell the GC that the Finalize process no longer needs to be run for this object.
            GC.SuppressFinalize(this);
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IBaseEntity
        {
            return _session.Query<TEntity>();
        }

        public IQueryable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class, IBaseEntity
        {
            return Query<TEntity>().Where(predicate);
        }

        #region Get
        public TEntity Get<TEntity>(Guid key) where TEntity : class, IBaseEntity
        {
            try
            {
                return _session.Get<TEntity>(key);
            }
            catch (ObjectNotFoundException)
            {
                return default(TEntity);
            }
        }
        public TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class, IBaseEntity
        {
            return Query<TEntity>().SingleOrDefault(predicate);
        }

        public Task GetAsync<TEntity>(Guid key) where TEntity : class, IBaseEntity
        {
            try
            {
                return _session.GetAsync<TEntity>(key);
            }
            catch (ObjectNotFoundException)
            {
                return Task.FromResult(default(TEntity));
            }
        }
        public Task GetAsync<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class, IBaseEntity
        {
            return Query<TEntity>().SingleOrDefaultAsync(predicate);
        }
        #endregion

        public IList<TEntity> GetPage<TEntity>(int pageIndex, int pageSize) where TEntity : class, IBaseEntity
        {
            try
            {
                ICriteria criteria = _session.CreateCriteria(typeof(TEntity));
                criteria.SetFirstResult(pageIndex * pageSize);
                if (pageSize > 0)
                {
                    criteria.SetMaxResults(pageSize);
                }
                return criteria.List<TEntity>();
            }
            catch (ObjectNotFoundException)
            {
                return new List<TEntity>();
            }
        }

        #region Load

        public TEntity Load<TEntity>(Guid primaryKey) where TEntity : class, IBaseEntity
        {
            try
            {
                return _session.Load<TEntity>(primaryKey);
            }
            catch (ObjectNotFoundException)
            {
                return default(TEntity);
            }
        }
        public Task LoadAsync<TEntity>(Guid primaryKey) where TEntity : class, IBaseEntity
        {
            try
            {
                return _session.LoadAsync<TEntity>(primaryKey);
            }
            catch (ObjectNotFoundException)
            {
                return Task.FromResult(default(TEntity));
            }
        }
        #endregion

        #region Insert
        public void Insert<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            Save(entity);
        }
        public void InsertAll<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                Save(obj);
        }
        public void InsertAll<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                Save(obj);
        }

        public Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            return SaveAsync(entity);
        }
        public Task InsertAllAsync<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                SaveAsync(obj);
            return Task.CompletedTask;
        }
        public Task InsertAllAsync<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                SaveAsync(obj);
            return Task.CompletedTask;
        }
        #endregion

        #region Save

        public TEntity Save<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            _session.Save(entity, entity.Id);
            return entity;
        }
        public TEntity SaveOrUpdate<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            _session.SaveOrUpdate(entity);
            return entity;
        }

        public Task SaveAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            return _session.SaveAsync(entity, entity.Id);
        }
        public Task SaveOrUpdateAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            return _session.SaveOrUpdateAsync(entity);
        }
        #endregion

        #region Update

        public TEntity Update<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            _session.Update(entity, entity.Id);
            return entity;
        }
        public void UpdateAll<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                Update(obj);
        }
        public void UpdateAll<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                Update(obj);
        }

        public Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            return _session.UpdateAsync(entity, entity.Id);
        }
        public Task UpdateAllAsync<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                UpdateAsync(obj);
            return Task.CompletedTask;
        }
        public Task UpdateAllAsync<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                UpdateAsync(obj);
            return Task.CompletedTask;
        }
        #endregion

        #region Delete
        public void Delete<TEntity>(Guid id) where TEntity : class, IBaseEntity
        {
            var entity = Get<TEntity>(id);
            Delete(entity);
        }
        public void Delete<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            _session.Delete(entity);
            //_session.Flush();
        }
        public void DeleteAll<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                Delete(obj);
        }
        public void DeleteAll<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                Delete(obj);
        }

        public Task DeleteAsync<TEntity>(Guid id) where TEntity : class, IBaseEntity
        {
            return _session.DeleteAsync(Load<TEntity>(id));
        }
        public Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            return _session.DeleteAsync(entity);
        }
        public Task DeleteAllAsync<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                DeleteAsync(obj);
            return Task.CompletedTask;
        }
        public Task DeleteAllAsync<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity
        {
            foreach (var obj in entities)
                DeleteAsync(obj);
            return Task.CompletedTask;
        }
        #endregion

        #region Protected Methods

        protected virtual void Dispose(bool disposeManagedResources)
        {
            if (_disposed) return; //already disposed
            if (!disposeManagedResources) return;
            if (!IsSafeSession) return;

            try
            {
                Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Rollback();
            }
            finally
            {
                if (IsSafeSession)
                {
                    _session.Close();
                    _session.Dispose();
                }
            }

            _disposed = true;
        }
        #endregion

        #region Private Methods

        void BeginTransaction()
        {
            if (!IsSafeSession) return;

            _session.BeginTransaction();
        }

        public void Commit()
        {
            if (!IsSafeSession) return;

            if (_session.Transaction != null &&
                _session.Transaction.IsActive &&
                !_session.Transaction.WasCommitted &&
                !_session.Transaction.WasRolledBack)
            {
                try
                {
                    _session.Transaction.Commit();
                }
                catch (Exception ex)
                {
                    Rollback();
                    throw;
                }
            }
            else
            {
                _session.Flush();
            }
        }
        public Task CommitAsync()
        {
            if (!IsSafeSession) return Task.FromCanceled(new CancellationToken());

            if (_session.Transaction != null &&
                _session.Transaction.IsActive &&
                !_session.Transaction.WasCommitted &&
                !_session.Transaction.WasRolledBack)
            {
                try
                {
                    return _session.Transaction.CommitAsync();
                }
                catch (Exception ex)
                {
                    Rollback();
                    return Task.FromException(ex);
                }
            }
            else
            {
                _session.Flush();
                return Task.FromCanceled(new CancellationToken());
            }
        }

        void Rollback()
        {
            if (!IsSafeSession) return;

            if (_session.Transaction != null && _session.Transaction.IsActive)
            {
                _session.Transaction.Rollback();
            }
        }
        #endregion

        #endregion Methods
    }
}
