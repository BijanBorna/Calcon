﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Calcon.Framework.Application.Models;
using Calcon.Framework.Core.Application.Models;

namespace Calcon.Framework.Data.NHRepository
{
    //Repository Interface  
    public interface IRepository
    {
        #region Operations 

        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IBaseEntity;

        IQueryable<TEntity> Where<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class, IBaseEntity;
        IList<TEntity> GetPage<TEntity>(int pageIndex, int pageSize) where TEntity : class, IBaseEntity;

        #region Get
        TEntity Get<TEntity>(Guid key) where TEntity : class, IBaseEntity;
        TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class, IBaseEntity;

        Task GetAsync<TEntity>(Guid key) where TEntity : class, IBaseEntity;
        Task GetAsync<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class, IBaseEntity;

        #endregion

        #region Load
        TEntity Load<TEntity>(Guid primaryKey) where TEntity : class, IBaseEntity;
        Task LoadAsync<TEntity>(Guid primaryKey) where TEntity : class, IBaseEntity;
        #endregion

        #region Insert

        void Insert<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        void InsertAll<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity;
        void InsertAll<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity;

        Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        Task InsertAllAsync<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity;
        Task InsertAllAsync<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity;
        #endregion

        #region Save
        TEntity Save<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        TEntity SaveOrUpdate<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;

        Task SaveAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        Task SaveOrUpdateAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        #endregion

        #region Update

        TEntity Update<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        void UpdateAll<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity;
        void UpdateAll<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity;
        Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        Task UpdateAllAsync<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity;
        Task UpdateAllAsync<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity;
        #endregion

        #region Delete

        void Delete<TEntity>(Guid id) where TEntity : class, IBaseEntity;
        void Delete<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        void DeleteAll<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity;
        void DeleteAll<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity;

        Task DeleteAsync<TEntity>(Guid id) where TEntity : class, IBaseEntity;
        Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;
        Task DeleteAllAsync<TEntity>(IList<TEntity> entities) where TEntity : class, IBaseEntity;
        Task DeleteAllAsync<TEntity>(params TEntity[] entities) where TEntity : class, IBaseEntity;
        #endregion
        #endregion Operations

        void Commit();
        Task CommitAsync();
        void Dispose();
    }
}
