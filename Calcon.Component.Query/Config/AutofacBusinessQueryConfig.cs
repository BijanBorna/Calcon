﻿using Autofac;
using Calcon.Component.Query.CompanyComponent;

namespace Calcon.Component.Query.Config
{
    public class AutofacBusinessQueryConfig : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(typeof(CompanyQuery).Assembly)
               .Where(t => t.Name.EndsWith("Query"))
               .AsImplementedInterfaces().InstancePerRequest();

        }
    }
}
