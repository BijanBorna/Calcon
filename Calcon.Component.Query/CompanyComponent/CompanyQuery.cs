﻿using System.Collections.Generic;
using System.Linq;
using Calcon.Component.Query.Contract.CompanyComponent;
using Calcon.DomainClasses.Domain;
using Calcon.DomainClasses.ValueType;
using Calcon.Framework.Application.Components;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Models;
using Calcon.ModelCalsses.ValueType;

namespace Calcon.Component.Query.CompanyComponent
{
    public class CompanyQuery : 
          QueryApplicationComponent<Company, CompanyModel>
        , ICompanyQuery
    {
        public CompanyQuery(IRepository repository) : base(repository)
        {
        }

        public IEnumerable<CompanyModel> GetAll()
        {
            return Query().Select(MapModel).AsEnumerable();
        }

        #region Mapping

        protected override CompanyModel MapModel(Company source)
        {
            if (source == null)
                return null;

            return new CompanyModel
            {
                Id = source.Id,
                Name = source.Name,
                Tel = source.Tel,
                Fax = source.Fax,
                Email = source.Email,
                WebSite = source.WebSite,
                Address = MapToAddress(source.Address),
                //Departments = source.Departments.Select(MapToDepartment).AsEnumerable()
            };
        }
        protected DepartmentModel MapToDepartment(Department model)
        {
            if (model == null) return null;

            return new DepartmentModel
            {
                Id = model.Id,
                Name = model.Name
            };
        }
        protected AddressModel MapToAddress(Address model)
        {
            if (model == null) return null;

            return new AddressModel
            {
                Id = model.Id,
                Place = model.Place
            };
        }

        #endregion Mapping

    }
}
