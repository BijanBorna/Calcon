﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calcon.Component.Query.Contract.DepartmentComponent;
using Calcon.DomainClasses.Domain;
using Calcon.Framework.Application.Components;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Models;

namespace Calcon.Component.Query.DepartmentComponent
{
    public class DepartmentQuery : QueryApplicationComponent<Department, DepartmentModel>, IDepartmentQuery
    {
        public DepartmentQuery(IRepository repository) : base(repository)
        {
        }

        public IEnumerable<DepartmentModel> GetAll()
        {
            return Query().Select(MapModel).AsEnumerable();
        }

        public IEnumerable<DepartmentModel> GetAll(Guid companyId)
        {
            return Query()
                .Where(x => x.Company.Id == companyId)
                .Select(MapModel)
                .AsEnumerable();
        }

        #region Mapping

        protected override DepartmentModel MapModel(Department source)
        {
            if (source == null)
                return null;

            return new DepartmentModel
            {
                Id = source.Id,
                Name = source.Name,
                Company = MapToDepartment(source.Company),
            };
        }
        protected CompanyModel MapToDepartment(Company model)
        {
            if (model == null) return null;

            return new CompanyModel
            {
                Id = model.Id,
                Name = model.Name
            };
        }
        #endregion Mapping
    }
}
