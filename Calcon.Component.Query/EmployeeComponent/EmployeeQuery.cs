﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calcon.Component.Query.Contract.EmployeeComponent;
using Calcon.DomainClasses.Domain;
using Calcon.DomainClasses.ValueType;
using Calcon.Framework.Application.Components;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Enums;
using Calcon.ModelCalsses.Models;
using Calcon.ModelCalsses.ValueType;

namespace Calcon.Component.Query.EmployeeComponent
{
    public class EmployeeQuery : QueryApplicationComponent<Employee, EmployeeModel>, IEmployeeQuery
    {
        public EmployeeQuery(IRepository repository) : base(repository)
        {
        }

        public IEnumerable<EmployeeModel> GetAll()
        {
            return Query().Select(MapModel).AsEnumerable();
        }

        public IEnumerable<EmployeeModel> GetAll(Guid departmentId)
        {
            return Query()
                .Where(x => x.Department.Id == departmentId)
                .Select(MapModel)
                .AsEnumerable();
        }

        #region Mapping

        protected override EmployeeModel MapModel(Employee source)
        {
            if (source == null)
                return null;

            return new EmployeeModel
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                Gender = (GenderModel)source.Gender,
                Birthdate = source.Birthdate,
                Tel = source.Tel,
                Mobile = source.Mobile,
                Email = source.Email,
                Address = MapToAddress(source.Address),
                Department = MapToDepartment(source.Department)
            };
        }
        protected DepartmentModel MapToDepartment(Department model)
        {
            if (model == null) return null;

            return new DepartmentModel
            {
                Id = model.Id,
                Name = model.Name
            };
        }
        protected AddressModel MapToAddress(Address model)
        {
            if (model == null) return null;

            return new AddressModel
            {
                Id = model.Id,
                Place = model.Place
            };
        }

        #endregion Mapping
    }
}
