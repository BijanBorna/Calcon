﻿using System.Collections.Generic;
using System.Linq;
using Calcon.Component.Query.Contract.UserComponent;
using Calcon.DomainClasses.Domain.User;
using Calcon.Framework.Application.Components;
using Calcon.Framework.Data.NHRepository;
using Calcon.ModelCalsses.Models.UserModel;

namespace Calcon.Component.Query.UserComponent
{
    public class UserQuery : 
          QueryApplicationComponent<User, UserModel>
        , IUserQuery
    {
        public UserQuery(IRepository repository) : base(repository)
        {
        }

        public IEnumerable<UserModel> GetAll()
        {
            return Query().Select(MapModel).AsEnumerable();
        }

        public UserModel GetByEmail(string email)
        {
            return MapModel(Query().FirstOrDefault(x => x.Email == email));
        }

        #region Mapping

        protected override UserModel MapModel(User source)
        {
            if (source == null)
                return null;

            return new UserModel
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName= source.LastName,
                Email = source.Email,
                HashedPassword= source.HashedPassword,
                Salt= source.Salt,
            };
        }

        #endregion Mapping
    }
}
