﻿using System;
using Calcon.Framework.Core.Domain.Entities;

namespace Calcon.Framework.Core.Application.Models
{
    public interface IBaseEntity
    {
        Guid Id { get; set; }
        DateTime CreationDate { get; }
        DateTime? ModificationDate { get; set; }
        EntityChangeState State { get; set; }
    }
}
