﻿using System;

namespace Calcon.Framework.Core.Application.Models
{
    public interface IBaseStructur
    {
        Guid Id { get; set; }
    }
}
