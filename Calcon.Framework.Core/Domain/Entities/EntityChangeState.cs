﻿namespace Calcon.Framework.Core.Domain.Entities
{
    public enum EntityChangeState
    {
        Orginal = 0,
        New = 1,
        Modified = 2,
        Deleted = 3
    }
}
