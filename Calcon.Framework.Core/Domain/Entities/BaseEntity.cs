﻿using System;
using Calcon.Framework.Core.Application.Models;

namespace Calcon.Framework.Core.Domain.Entities
{
    public abstract class BaseEntity : IBaseEntity
    {
        public virtual Guid Id { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual DateTime? ModificationDate { get; set; }
        public virtual EntityChangeState State { get; set; }

        #region Public Methods

        public BaseEntity()
        {
            CreationDate = DateTime.Now;
        }
        public override int GetHashCode()
        {
            if (IsTransient())
                return base.GetHashCode();

            unchecked
            {
                var hash = GetType().GetHashCode();
                return (hash * 31) ^ Id.GetHashCode();
            }
        }

        public virtual bool IsTransient()
        {
            return Id == default(Guid);
        }

        protected bool Equals(BaseEntity other)
        {
            return Id.Equals(other.Id);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((BaseEntity)obj);
        }

        public override string ToString()
        {
            return $"[{GetType().Name} : {Id}]";
        }

        #endregion

        #region Operators

        public static bool operator ==(BaseEntity left, BaseEntity right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(BaseEntity left, BaseEntity right)
        {
            return !(left == right);
        }

        #endregion
    }
}
