﻿using Calcon.Framework.Core.Application.Components;
using Calcon.ModelCalsses.Models;

namespace Calcon.Component.Command.Contract.Company
{
    public interface ICompanyCommand : ICommandApplicationComponent<CompanyModel>
    {
    }
}
