﻿using Calcon.Framework.Core.Application.Components;
using Calcon.ModelCalsses.Models;

namespace Calcon.Component.Command.Contract.Department
{
    public interface IDepartmentCommand : ICommandApplicationComponent<DepartmentModel>
    {
    }
}
