﻿using Calcon.Framework.Core.Application.Components;
using Calcon.ModelCalsses.Models;

namespace Calcon.Component.Command.Contract.Employee
{
    public interface IEmployeeCommand : ICommandApplicationComponent<EmployeeModel>
    {
    }
}
