﻿using Calcon.Framework.Core.Application.Components;
using Calcon.ModelCalsses.Models.UserModel;

namespace Calcon.Component.Command.Contract.User
{
    public interface IUserCommand : ICommandApplicationComponent<UserModel>
    {
    }
}
