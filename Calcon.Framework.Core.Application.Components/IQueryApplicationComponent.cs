﻿using System;
using System.Threading.Tasks;
using Calcon.Framework.Core.Adapter;

namespace Calcon.Framework.Core.Application.Components
{
    public interface IQueryApplicationComponent<out TModel>
        where TModel : class, IParam
    {
        TModel GetById(Guid id);
        bool Exists(Guid id);
        Task<bool> ExistsAsync(Guid id);
    }
}
