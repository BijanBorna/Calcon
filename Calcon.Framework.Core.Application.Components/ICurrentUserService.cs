﻿using System;
using Calcon.Framework.Core.Dependency;

namespace Calcon.Framework.Core.Application.Components
{
    public interface ICurrentUserService : ITransientDependency
    {
        Guid Id { get; }
        string UserName { get; }
        string DisplayName { get; }
        string BrowserName { get; }
        string Ip { get; }
        bool IsAuthenticated { get; }
    }
}
