﻿using System;
using System.Threading.Tasks;
using Calcon.Framework.Core.Adapter;

namespace Calcon.Framework.Core.Application.Components
{
    public interface ICommandApplicationComponent<in TParam> : IBaseComponent
        where TParam : class, IParam
    {
        #region Create

        bool Create(TParam model);
        Task CreateAsync(TParam model);

        #endregion

        #region Edit

        bool Edit(TParam model);
        Task EditAsync(TParam model);

        #endregion

        #region Delete

        bool Delete(Guid id);
        Task DeleteAsync(Guid id);

        #endregion

        void CommitChange();
        Task CommitChangeAsync();
    }
}
