﻿using Calcon.Framework.Core.Dependency;

namespace Calcon.Framework.Core.Application.Components
{
    public interface IBaseComponent : ITransientDependency
    {
    }
}
