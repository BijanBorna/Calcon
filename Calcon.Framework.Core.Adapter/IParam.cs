﻿using System;

namespace Calcon.Framework.Core.Adapter
{
    public interface IParam
    {
        Guid Id { get; set; }
    }
}
