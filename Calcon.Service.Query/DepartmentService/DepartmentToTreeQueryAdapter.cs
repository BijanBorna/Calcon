﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.PublicModels;

namespace Calcon.Service.Query.DepartmentService
{
    public class DepartmentToTreeQueryAdapter : IQueryAdapter<DepartmentModel, TreeViewModel>
    {
        public TreeViewModel ToMap(DepartmentModel source)
        {
            return new TreeViewModel
            {
                id = source.Id,
                title = source.Name,
                path = "employee",
                parentId = source.Company.Id,
                type = "Department"
            };
        }
    }
}
