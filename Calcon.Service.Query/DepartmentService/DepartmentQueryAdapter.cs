﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Query.DepartmentService
{
    public class DepartmentQueryAdapter : IQueryAdapter<DepartmentModel, DepartmentViewModel>
    {
        public DepartmentViewModel ToMap(DepartmentModel source)
        {
            return new DepartmentViewModel
            {
                Id = source.Id,
                Name = source.Name,
                CompanyId = source.Company.Id
            };
        }
    }
}
