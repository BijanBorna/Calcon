﻿using Calcon.Service.Query.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using Calcon.Component.Query.Contract.DepartmentComponent;
using Calcon.ViewModelClasses.PublicModels;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Query.DepartmentService
{
    public class DepartmentQueryService : IDepartmentQueryService
    {
        private readonly IDepartmentQuery _departmentQuery;
        public DepartmentQueryService(IDepartmentQuery departmentQuery)
        {
            _departmentQuery = departmentQuery;
        }

        public DepartmentViewModel Get(Guid id)
        {
            var department = _departmentQuery.GetById(id);
            return new DepartmentQueryAdapter().ToMap(department);
        }

        public List<DepartmentViewModel> GetAll()
        {
            var mapper = new DepartmentQueryAdapter();
            var departments = _departmentQuery
                .GetAll()
                .ToList()
                .Select(x => mapper.ToMap(x));
            return departments.ToList();
        }

        public List<TreeViewModel> GetForTree(Guid comapnyId)
        {
            var mapper = new DepartmentToTreeQueryAdapter();
            var departmentTree = _departmentQuery
                .GetAll(comapnyId)
                .ToList()
                .Select(x => mapper.ToMap(x));
            return departmentTree.ToList();
        }

        public IEnumerable<ComboViewModel> GetForCombo()
        {
            var mapper = new DepartmentToComboQueryAdapter();
            var departmentCombo = _departmentQuery
                .GetAll()
                .ToList()
                .Select(x => mapper.ToMap(x));
            return departmentCombo;
        }

        public IEnumerable<ComboViewModel> GetForCombo(Guid comapnyId)
        {
            var mapper = new DepartmentToComboQueryAdapter();
            var departmentCombo = _departmentQuery
                .GetAll(comapnyId)
                .ToList()
                .Select(x => mapper.ToMap(x));
            return departmentCombo;
        }
    }
}
