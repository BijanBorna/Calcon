﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.PublicModels;

namespace Calcon.Service.Query.DepartmentService
{
    public class DepartmentToComboQueryAdapter : IQueryAdapter<DepartmentModel, ComboViewModel>
    {
        public ComboViewModel ToMap(DepartmentModel source)
        {
            return new ComboViewModel
            {
                Id = source.Id,
                Title = source.Name
            };
        }
    }
}
