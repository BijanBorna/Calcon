﻿using Calcon.Service.Query.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using Calcon.Component.Query.Contract.EmployeeComponent;
using Calcon.ViewModelClasses.PublicModels;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Query.EmployeeService
{
    public class EmployeeQueryService : IEmployeeQueryService
    {
        private readonly IEmployeeQuery _employeeQuery;
        public EmployeeQueryService(IEmployeeQuery employeeQuery)
        {
            _employeeQuery = employeeQuery;
        }

        public EmployeeViewModel Get(Guid id)
        {
            var employee = _employeeQuery.GetById(id);
            return new EmployeeQueryAdapter().ToMap(employee);
        }

        public List<EmployeeViewModel> GetAll()
        {
            var mapper = new EmployeeQueryAdapter();
            var employees = _employeeQuery
                .GetAll()
                .ToList()
                .Select(x => mapper.ToMap(x));
            return employees.ToList();
        }

        public List<TreeViewModel> GetForTree(Guid departmentId)
        {
            var mapper = new EmployeeToTreeQueryAdapter();
            var employeeTree = _employeeQuery
                .GetAll(departmentId)
                .ToList()
                .Select(x => mapper.ToMap(x));
            return employeeTree.ToList();
        }
    }
}
