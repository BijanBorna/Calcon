﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.Enums;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Query.EmployeeService
{
    public class EmployeeQueryAdapter : IQueryAdapter<EmployeeModel, EmployeeViewModel>
    {
        public EmployeeViewModel ToMap(EmployeeModel source)
        {
            return new EmployeeViewModel
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                Gender = (GenderViewModel)source.Gender,
                Birthdate = source.Birthdate,
                Tel = source.Tel,
                Mobile = source.Mobile,
                Email = source.Email,
                DepartmentId = source.Department.Id
            };
        }
    }
}
