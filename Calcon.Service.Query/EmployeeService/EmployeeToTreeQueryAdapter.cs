﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.PublicModels;

namespace Calcon.Service.Query.EmployeeService
{
    public class EmployeeToTreeQueryAdapter : IQueryAdapter<EmployeeModel, TreeViewModel>
    {
        public TreeViewModel ToMap(EmployeeModel source)
        {
            return new TreeViewModel
            {
                id = source.Id,
                title = $"{source.FirstName} {source.LastName}",
                isLast = true,
                parentId = source.Department.Id,
                type = "Employee"
            };
        }
    }
}
