﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.PublicModels;

namespace Calcon.Service.Query.CompanyService
{
    public class CompanyToComboQueryAdapter : IQueryAdapter<CompanyModel, ComboViewModel>
    {
        public ComboViewModel ToMap(CompanyModel source)
        {
            return new ComboViewModel
            {
                Id = source.Id,
                Title = source.Name
            };
        }
    }
}
