﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Query.CompanyService
{
    public class CompanyQueryAdapter : IQueryAdapter<CompanyModel, CompanyViewModel>
    {
        public CompanyViewModel ToMap(CompanyModel source)
        {
            return new CompanyViewModel
            {
                Id = source.Id,
                Name = source.Name,
                Email = source.Email,
                //Address = source.
                Fax = source.Fax,
                Tel = source.Tel,
                WebSite = source.WebSite
            };
        }
    }
}
