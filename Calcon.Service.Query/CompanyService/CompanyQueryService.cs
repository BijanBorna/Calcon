﻿using Calcon.Service.Query.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using Calcon.Component.Query.Contract.CompanyComponent;
using Calcon.ViewModelClasses.ViewModels;
using Calcon.ViewModelClasses.PublicModels;

namespace Calcon.Service.Query.CompanyService
{
    public class CompanyQueryService : ICompanyQueryService
    {
        private readonly ICompanyQuery _companyQuery;
        public CompanyQueryService(ICompanyQuery companyQuery)
        {
            _companyQuery = companyQuery;
        }

        public CompanyViewModel Get(Guid id)
        {
            var company = _companyQuery.GetById(id);
            return new CompanyQueryAdapter().ToMap(company);
        }

        public List<CompanyViewModel> GetAll()
        {
            var mapper = new CompanyQueryAdapter();
            var company = _companyQuery
                .GetAll()
                .ToList()
                .Select(x => mapper.ToMap(x));
            return company.ToList();
        }

        public List<TreeViewModel> GetForTree()
        {
            var mapper = new CompanyToTreeQueryAdapter();
            var companyTree = _companyQuery
                .GetAll()
                .ToList()
                .Select(x => mapper.ToMap(x));
            return companyTree.ToList();
        }

        public IEnumerable<ComboViewModel> GetForCombo()
        {
            var mapper = new CompanyToComboQueryAdapter();
            var companyCombo = _companyQuery
                .GetAll()
                .ToList()
                .Select(x => mapper.ToMap(x));
            return companyCombo;
        }
    }
}
