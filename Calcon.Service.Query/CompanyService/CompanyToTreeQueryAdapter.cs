﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.PublicModels;

namespace Calcon.Service.Query.CompanyService
{
    public class CompanyToTreeQueryAdapter : IQueryAdapter<CompanyModel, TreeViewModel>
    {
        public TreeViewModel ToMap(CompanyModel source)
        {
            return new TreeViewModel
            {
                id = source.Id,
                title = source.Name,
                path = "department",
                type = "Company"
            };
        }
    }
}
