﻿using Calcon.ModelCalsses.Models.UserModel;
using Calcon.Service.Query.Contract;
using Calcon.ViewModelClasses.ViewModels.UserViewModel;

namespace Calcon.Service.Query.UserService
{
    public class UserQueryAdapter : IQueryAdapter<UserModel, UserViewModel>
    {
        public UserViewModel ToMap(UserModel source)
        {
            if (source == null)
                return null;

            return new UserViewModel
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                Email = source.Email,
                HashedPassword = source.HashedPassword,
                Salt = source.Salt
            };
        }
    }
}
