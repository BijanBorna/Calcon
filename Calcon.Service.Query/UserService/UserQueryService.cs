﻿using Calcon.Service.Query.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Calcon.Common;
using Calcon.Common.EncryptDecryptHelper;
using Calcon.Component.Query.Contract.UserComponent;
using Calcon.ViewModelClasses.ViewModels.UserViewModel;

namespace Calcon.Service.Query.UserService
{
    public class UserQueryService : IUserQueryService
    {
        private readonly IUserQuery _userQuery;
        public UserQueryService(IUserQuery userQuery)
        {
            _userQuery = userQuery;
        }

        public UserViewModel GetUser(Guid id)
        {
            var user = _userQuery.GetById(id);
            return new UserQueryAdapter().ToMap(user);
        }

        public List<UserViewModel> GetAllUser()
        {
            var mapper = new UserQueryAdapter();
            var user = _userQuery
                .GetAll()
                .ToList()
                .Select(x => mapper.ToMap(x));
            return user.ToList();
        }

        public UserViewModel GetUserByEmail(string email)
        {
            var mapper = new UserQueryAdapter();
            return mapper.ToMap(_userQuery.GetByEmail(email));
        }

        public MembershipContext ValidateUser(string email, string password)
        {
            var membershipCtx = new MembershipContext();
            var userParam = GetUserByEmail(email);
            if (userParam != null && IsUserValid(userParam, password))
            {
                membershipCtx.User = userParam;
                var identity = new GenericIdentity(userParam.Email);
                membershipCtx.Principal = new GenericPrincipal(identity, new []{"Admin"});
            }
            if (membershipCtx.User == null)
            {
                throw new Exception("User name or password is invalid.");
            }
            return membershipCtx;
        }
        #region [Private Method]

        private bool IsPasswordValid(UserViewModel user, string password) => string.Equals(EncryptionHelper.EncryptPassword(password, user.Salt), user.HashedPassword);
        private bool IsUserValid(UserViewModel user, string password)
        {
            return (IsPasswordValid(user, password));
        }
        #endregion
    }
}
