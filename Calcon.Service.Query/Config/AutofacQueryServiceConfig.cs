﻿using Autofac;
using Calcon.Service.Query.CompanyService;
using Calcon.Component.Query.Config;

namespace Calcon.Service.Query.Config
{
   public class AutofacQueryServiceConfig : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            // Service
            //builder.RegisterAssemblyModules(typeof(AutofacBusinessCommandConfig).Assembly);
            builder.RegisterAssemblyModules(typeof(AutofacBusinessQueryConfig).Assembly);

            builder.RegisterAssemblyTypes(typeof(CompanyQueryService).Assembly)
               .Where(t => t.Name.EndsWith("QueryService"))
               .AsImplementedInterfaces().InstancePerRequest();
            builder.RegisterAssemblyTypes(typeof(CompanyQueryAdapter).Assembly)
               .Where(t => t.Name.EndsWith("QueryAdapter"))
               .AsImplementedInterfaces().InstancePerRequest();
        }
    }
}
