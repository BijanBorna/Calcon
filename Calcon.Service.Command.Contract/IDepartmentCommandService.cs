﻿using System;
using Calcon.Framework.Service.Core;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Command.Contract
{
    public interface IDepartmentCommandService : ICommandService
    {
        bool Insert(DepartmentViewModel model);
        bool Update(DepartmentViewModel model);
        bool Delete(Guid id);
    }
}
