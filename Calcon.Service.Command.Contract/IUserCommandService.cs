﻿using System;
using Calcon.ViewModelClasses.ViewModels.UserViewModel;

namespace Calcon.Service.Command.Contract
{
    public interface IUserCommandService : ICommandService
    {
        void Create(UserViewModel user);
        bool Update(UserViewModel model);
        bool Remove(Guid id);
    }
}
