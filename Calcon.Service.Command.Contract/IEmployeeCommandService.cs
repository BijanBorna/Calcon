﻿using System;
using Calcon.Framework.Service.Core;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Command.Contract
{
    public interface IEmployeeCommandService : ICommandService
    {
        bool Insert(EmployeeViewModel model);
        bool Update(EmployeeViewModel model);
        bool Delete(Guid id);
    }
}
