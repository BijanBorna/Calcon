﻿using System;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Command.Contract
{
    public interface ICompanyCommandService : ICommandService
    {
        bool Insert(CompanyViewModel model);
        bool Update(CompanyViewModel model);
        bool Delete(Guid id);
    }
}
