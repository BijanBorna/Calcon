﻿namespace Calcon.Service.Command.Contract
{
    public interface ICommandAdapter<in TSource,out TDestination>
    {
        TDestination ToMap(TSource source);
    }
}
