﻿using Calcon.Framework.Core.Dependency;

namespace Calcon.Service.Command.Contract
{
    public interface ICommandService : ITransientDependency
    {
    }
}
