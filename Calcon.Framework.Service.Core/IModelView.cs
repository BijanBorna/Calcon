﻿using System;

namespace Calcon.Framework.Service.Core
{
    public interface IViewModel
    {
        Guid Id { get; set; }
    }
}
