﻿using Calcon.DomainClasses.Domain;
using FluentNHibernate.Mapping;

namespace Calcon.DomainClasses.Mapping
{
    public class CompanyMap : ClassMap<Company>
    {
        public CompanyMap()
        {
            //Schema("Common");

            Id(p => p.Id)
                .GeneratedBy.Guid();
            //.Column("Id")
            //;

            Map(p => p.Name).Length(50);
            Map(p => p.Family).Length(50);
            Map(m => m.Address).Length(250);
            HasMany(o => o.Departments)
               .AsList(index => index.Column("ListIndex"));
            //References(r => r.Departments).Cascade(Department);//.Not.Nullable();
        }
    }
}
