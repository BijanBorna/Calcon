﻿using Calcon.DomainClasses.Domain;
using FluentNHibernate.Mapping;

namespace Calcon.DomainClasses.Mapping
{
    public class DepartmentMap : ClassMap<Department>
    {
        public DepartmentMap()
        {
            //Schema("Common");

            Id(p => p.Id)
                .GeneratedBy.Guid();
            //.Column("Id")
            //;

            Map(p => p.FirstName).Not.Nullable().Length(50);
            Map(p => p.LastName).Length(50);
            References(r => r.Company).Not.Nullable();
        }
    }
}
