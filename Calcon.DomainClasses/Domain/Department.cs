﻿using System.Collections.Generic;
using Calcon.Framework.Core.Domain.Entities;

namespace Calcon.DomainClasses.Domain
{
    public class Department : BaseEntity
    {
        public virtual string Name { get; set; }
        public virtual Company Company { get; set; }
        public virtual IEnumerable<Employee> Employees { get; set; }

        public Department()
        {
            Employees = new List<Employee>();
        }
    }
}
