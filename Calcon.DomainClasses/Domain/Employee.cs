﻿using System;
using Calcon.DomainClasses.Enums;
using Calcon.DomainClasses.ValueType;
using Calcon.Framework.Core.Domain.Entities;

namespace Calcon.DomainClasses.Domain
{
    public class Employee : BaseEntity
    {
        //[Length(Min = 3, Max = 20,Message="Between 3 and 20")]
        public virtual string FirstName { get; set; }
        public virtual string MiddleName { get; set; }

        //[Length(Min = 3, Max = 60, Message = "Between 3 and 60")]
        public virtual string LastName { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual DateTime? Birthdate { get; set; }
        public virtual Address Address { get; set; }
        public virtual string Tel { get; set; }
        public virtual string Mobile { get; set; }
        public virtual string Email { get; set; }

        public virtual Department Department { get; set; }
    }
}
