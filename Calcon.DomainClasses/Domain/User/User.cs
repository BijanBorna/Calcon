﻿using Calcon.Framework.Core.Domain.Entities;

namespace Calcon.DomainClasses.Domain.User
{
    public class User : BaseEntity
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Email { get; set; }
        public string Salt { get; set; }
        public string HashedPassword { get; set; }
    }
}
