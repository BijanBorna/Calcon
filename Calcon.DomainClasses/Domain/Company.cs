﻿using System.Collections.Generic;
using Calcon.DomainClasses.ValueType;
using Calcon.Framework.Core.Domain.Entities;

namespace Calcon.DomainClasses.Domain
{
    public class Company : BaseEntity
    {
        public virtual string Name { get; set; }
        public virtual string Tel { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Email { get; set; }
        public virtual string WebSite { get; set; }
        public virtual Address Address { get; set; }
        public virtual IEnumerable<Department> Departments { get; set; }

        public Company()
        {
            Departments = new List<Department>();
        }
    }
}
