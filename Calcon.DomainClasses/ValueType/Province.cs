﻿using Calcon.Framework.Core.Domain.Entities;

namespace Calcon.DomainClasses.ValueType
{
    public class Province : BaseEntity
    {
        public string Name { get; set; }
        public Country Country { get; set; }
    }
}
