﻿using System.Collections.Generic;
using Calcon.Framework.Core.Domain.Entities;

namespace Calcon.DomainClasses.ValueType
{
    public class Country : BaseEntity
    {
        public string Name { get; set; }
        public List<Province> Provinces { get; set; }
    }
}
