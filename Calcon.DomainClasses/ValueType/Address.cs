﻿using Calcon.DomainClasses.Domain;
using Calcon.Framework.Core.Domain.Entities;

namespace Calcon.DomainClasses.ValueType
{
    public class Address : BaseEntity
    {
        public string Place { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Province { get; set; }
        //public Country Country { get; set; }
        //public Province Province { get; set; }
        //public Company Company { get; set; }
    }
}
