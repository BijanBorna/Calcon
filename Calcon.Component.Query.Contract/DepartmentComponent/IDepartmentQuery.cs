﻿using System;
using System.Collections.Generic;
using Calcon.Framework.Core.Application.Components;
using Calcon.ModelCalsses.Models;

namespace Calcon.Component.Query.Contract.DepartmentComponent
{
    public interface IDepartmentQuery : IQueryApplicationComponent<DepartmentModel>
    {
        IEnumerable<DepartmentModel> GetAll();
        IEnumerable<DepartmentModel> GetAll(Guid companyId);
    }
}
