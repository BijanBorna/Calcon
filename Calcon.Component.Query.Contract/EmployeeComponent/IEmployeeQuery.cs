﻿using System;
using System.Collections.Generic;
using Calcon.Framework.Core.Application.Components;
using Calcon.ModelCalsses.Models;

namespace Calcon.Component.Query.Contract.EmployeeComponent
{
    public interface IEmployeeQuery : IQueryApplicationComponent<EmployeeModel>
    {
        IEnumerable<EmployeeModel> GetAll();
        IEnumerable<EmployeeModel> GetAll(Guid departmentId);
    }
}
