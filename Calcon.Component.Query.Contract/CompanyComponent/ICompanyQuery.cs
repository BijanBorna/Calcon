﻿using System.Collections.Generic;
using Calcon.Framework.Core.Application.Components;
using Calcon.ModelCalsses.Models;

namespace Calcon.Component.Query.Contract.CompanyComponent
{
    public interface ICompanyQuery : IQueryApplicationComponent<CompanyModel>
    {
        IEnumerable<CompanyModel> GetAll();
    }
}
