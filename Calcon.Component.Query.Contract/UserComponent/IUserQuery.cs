﻿using System.Collections.Generic;
using Calcon.Framework.Core.Application.Components;
using Calcon.ModelCalsses.Models.UserModel;

namespace Calcon.Component.Query.Contract.UserComponent
{
    public interface IUserQuery : IQueryApplicationComponent<UserModel>
    {
        IEnumerable<UserModel> GetAll();
        UserModel GetByEmail(string email);
    }
}
