﻿using Calcon.DomainClasses.Domain.User;
using NHibernate.Validator.Cfg.Loquacious;

namespace Calcon.Validation.Validation
{
    public class UserValidatio : ValidationDef<User>
    {
        public UserValidatio()
        {
            Define(x => x.FirstName)
                .NotNullableAndNotEmpty().WithMessage("First Name not empty").And
                .LengthBetween(3, 50).WithMessage("First name should be between 3 and 50.");

            Define(x => x.LastName)
                .NotNullableAndNotEmpty().WithMessage("Last Name not empty").And
                .LengthBetween(3, 60).WithMessage("Last name should be between 3 and 20.");

            Define(x => x.Email)
                .NotNullableAndNotEmpty().WithMessage("Email not empty").And
                .IsEmail().WithMessage("Email is incorrect");
        }
    }
}
