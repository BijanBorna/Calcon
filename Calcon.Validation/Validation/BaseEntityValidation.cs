﻿using Calcon.Framework.Core.Domain.Entities;
using NHibernate.Validator.Cfg.Loquacious;

namespace Calcon.Validation.Validation
{
    public class BaseEntityValidation : ValidationDef<BaseEntity>
    {
        public BaseEntityValidation()
        {
            Define(x => x.Id).NotEmpty();
            Define(x => x.State).NotNullable();
        }
    }
}
