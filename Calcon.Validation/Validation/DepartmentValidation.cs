﻿using Calcon.DomainClasses.Domain;
using NHibernate.Validator.Cfg.Loquacious;

namespace Calcon.Validation.Validation
{
    public class DepartmentValidation : ValidationDef<Department>
    {
        public DepartmentValidation()
        {
            Define(x => x.Name)
                .NotNullableAndNotEmpty().WithMessage("Name not empty").And
                .LengthBetween(3, 50).WithMessage("Department name should be between 3 and 50.");

            Define(x => x.Company)
                .NotNullable().WithMessage("You should choose a company");
        }
    }
}
