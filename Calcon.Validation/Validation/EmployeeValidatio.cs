﻿using Calcon.DomainClasses.Domain;
using NHibernate.Validator.Cfg.Loquacious;

namespace Calcon.Validation.Validation
{
    public class EmployeeValidatio : ValidationDef<Employee>
    {
        public EmployeeValidatio()
        {
            Define(x => x.FirstName)
                .NotNullableAndNotEmpty().WithMessage("First Name not empty").And
                .LengthBetween(3, 50).WithMessage("First name should be between 3 and 50.");

            Define(x => x.LastName)
                .NotNullableAndNotEmpty().WithMessage("Last Name not empty").And
                .LengthBetween(3, 60).WithMessage("Last name should be between 3 and 20.");

            Define(x => x.Gender)
                .NotNullable().WithMessage("Gender not empty");

            Define(x => x.Department)
                .NotNullable().WithMessage("You should choose a department");
        }
    }
}
