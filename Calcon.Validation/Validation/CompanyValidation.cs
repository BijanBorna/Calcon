﻿using Calcon.DomainClasses.Domain;
using NHibernate.Validator.Cfg.Loquacious;

namespace Calcon.Validation.Validation
{
    public class CompanyValidation : ValidationDef<Company>
    {
        public CompanyValidation()
        {
            Define(x => x.Name)
                .LengthBetween(3, 50)
                .WithMessage("Company name should be between 3 and 50.");

            Define(x => x.Tel)
                .NotNullableAndNotEmpty().WithMessage("Phone not empty").And
                .LengthBetween(8, 15).WithMessage("Phone should be between 8 and 15.").And
                .IsNumeric().WithMessage("Should be integer");

            Define(x => x.Email)
                .NotNullableAndNotEmpty().WithMessage("Email not empty").And
                .IsEmail().WithMessage("Email is incorrect");
        }
    }
}
