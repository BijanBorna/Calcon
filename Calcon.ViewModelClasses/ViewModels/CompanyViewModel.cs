﻿namespace Calcon.ViewModelClasses.ViewModels
{
    public class CompanyViewModel : ViewModel
    {
        public string Name { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        //public AddressModel Address { get; set; }
        //public IList<DepartmentViewModel> Departments { get; set; }

        //public CompanyViewModel()
        //{
        //    Departments = new List<DepartmentViewModel>();
        //}
    }
}
