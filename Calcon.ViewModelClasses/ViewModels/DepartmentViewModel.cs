﻿using System;

namespace Calcon.ViewModelClasses.ViewModels
{
    public class DepartmentViewModel : ViewModel
    {
        public string Name { get; set; }
        public Guid CompanyId { get; set; }
    }
}
