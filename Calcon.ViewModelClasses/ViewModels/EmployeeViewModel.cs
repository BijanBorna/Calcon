﻿using System;
using System.Collections.Generic;
using Calcon.ViewModelClasses.Enums;

namespace Calcon.ViewModelClasses.ViewModels
{
    public class EmployeeViewModel : ViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public GenderViewModel Gender { get; set; }
        public DateTime? Birthdate { get; set; }
        public string Tel { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        //public virtual AddressModel Address { get; set; }
        public Guid DepartmentId { get; set; }
    }
}
