﻿using System;
using Calcon.Framework.Service.Core;

namespace Calcon.ViewModelClasses
{
    public class ViewModel : IViewModel
    {
        public Guid Id { get; set; }
    }
}
