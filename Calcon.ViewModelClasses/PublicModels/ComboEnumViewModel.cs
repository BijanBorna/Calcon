﻿namespace Calcon.ViewModelClasses.PublicModels
{
    public class ComboEnumViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
