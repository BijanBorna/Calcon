﻿using System;

namespace Calcon.ViewModelClasses.PublicModels
{
    public class ComboViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}
