﻿using System;
using System.Collections.Generic;

namespace Calcon.ViewModelClasses.PublicModels
{
    public class TreeViewModel //: ViewModel
    {
        public Guid id { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string path { get; set; }
        public bool isLast { get; set; }
        public Guid? parentId { get; set; }
        public List<TreeViewModel> nodes { get; set; }

        public TreeViewModel()
        {
            nodes = new List<TreeViewModel>();
        }
    }
}
