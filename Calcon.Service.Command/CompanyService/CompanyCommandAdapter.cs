﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Command.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Command.CompanyService
{
    public class CompanyCommandAdapter : ICommandAdapter<CompanyViewModel, CompanyModel>
    {
        public CompanyModel ToMap(CompanyViewModel source)
        {
            return new CompanyModel
            {
                Id = source.Id,
                Name = source.Name,
                Email = source.Email,
                //Address = source.
                Fax = source.Fax,
                Tel = source.Tel,
                WebSite = source.WebSite
            };
        }
    }
}
