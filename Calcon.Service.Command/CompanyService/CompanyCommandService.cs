﻿using System;
using Calcon.Component.Command.Contract.Company;
using Calcon.Service.Command.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Command.CompanyService
{
    public class CompanyCommandService : ICompanyCommandService
    {
        private readonly ICompanyCommand _companyCommand;
        public CompanyCommandService(ICompanyCommand companyCommand)
        {
            _companyCommand = companyCommand;
        }
        public bool Insert(CompanyViewModel model)
        {
            _companyCommand.Create(new CompanyCommandAdapter().ToMap(model));
            _companyCommand.CommitChange();
            return true;
        }

        public bool Update(CompanyViewModel model)
        {
            _companyCommand.Edit(new CompanyCommandAdapter().ToMap(model));
            _companyCommand.CommitChange();
            return true;
        }

        public bool Delete(Guid id)
        {
            _companyCommand.Delete(id);
            _companyCommand.CommitChange();
            return true;
        }
    }
}
