﻿using Calcon.ModelCalsses.Models.UserModel;
using Calcon.Service.Command.Contract;
using Calcon.ViewModelClasses.ViewModels.UserViewModel;

namespace Calcon.Service.Command.UserService
{
    public class UserCommandAdapter : ICommandAdapter<UserViewModel, UserModel>
    {
        public UserModel ToMap(UserViewModel source)
        {
            return new UserModel
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                Email = source.Email,
                HashedPassword = source.HashedPassword,
                Salt = source.Salt
            };
        }
    }
}
