﻿using System;
using Calcon.Component.Command.Contract.User;
using Calcon.Service.Command.Contract;
using Calcon.ViewModelClasses.ViewModels.UserViewModel;
using Calcon.Common.EncryptDecryptHelper;

namespace Calcon.Service.Command.UserService
{
    public class UserCommandService : IUserCommandService
    {
        private readonly IUserCommand _userCommand;
        public UserCommandService(IUserCommand userCommand)
        {
            _userCommand = userCommand;
        }
        public void Create(UserViewModel model)
        {

            var passwordSalt = EncryptionHelper.CreateSalt();
            model.HashedPassword = EncryptionHelper.EncryptPassword(model.Password, passwordSalt);
            model.Salt = passwordSalt;

            _userCommand.Create(new UserCommandAdapter().ToMap(model));
            _userCommand.CommitChange();
        }

        public bool Update(UserViewModel model)
        {
            _userCommand.Edit(new UserCommandAdapter().ToMap(model));
            _userCommand.CommitChange();
            return true;
        }

        public bool Remove(Guid id)
        {
            _userCommand.Delete(id);
            _userCommand.CommitChange();
            return true;
        }
    }
}
