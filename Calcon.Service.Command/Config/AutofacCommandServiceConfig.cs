﻿using Autofac;
using Calcon.Component.Command.Config;
using Calcon.Service.Command.CompanyService;
using Calcon.Component.Query.Config;

namespace Calcon.Service.Command.Config
{
   public class AutofacCommandServiceConfig : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            // Service
            builder.RegisterAssemblyModules(typeof(AutofacBusinessCommandConfig).Assembly);
            //builder.RegisterAssemblyModules(typeof(AutofacBusinessQueryConfig).Assembly);

            builder.RegisterAssemblyTypes(typeof(CompanyCommandService).Assembly)
               .Where(t => t.Name.EndsWith("CommandService"))
               .AsImplementedInterfaces().InstancePerRequest();
            //builder.RegisterAssemblyTypes(typeof(CompanyCommandAdapter).Assembly)
            //   .Where(t => t.Name.EndsWith("CommandAdapter"))
            //   .AsImplementedInterfaces().InstancePerRequest();

        }
    }
}
