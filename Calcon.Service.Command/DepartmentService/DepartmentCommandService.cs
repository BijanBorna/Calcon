﻿using System;
using Calcon.Component.Command.Contract.Department;
using Calcon.Service.Command.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Command.DepartmentService
{
    public class DepartmentCommandService : IDepartmentCommandService
    {
        private readonly IDepartmentCommand _departmentCommand;
        public DepartmentCommandService(IDepartmentCommand companyCommand)
        {
            _departmentCommand = companyCommand;
        }
        public bool Insert(DepartmentViewModel model)
        {
            _departmentCommand.Create(new DepartmentCommandAdapter().ToMap(model));
            _departmentCommand.CommitChange();
            return true;
        }

        public bool Update(DepartmentViewModel model)
        {
            _departmentCommand.Edit(new DepartmentCommandAdapter().ToMap(model));
            _departmentCommand.CommitChange();
            return true;
        }

        public bool Delete(Guid id)
        {
            _departmentCommand.Delete(id);
            _departmentCommand.CommitChange();
            return true;
        }
    }
}
