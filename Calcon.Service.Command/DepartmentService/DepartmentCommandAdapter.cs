﻿using Calcon.ModelCalsses.Models;
using Calcon.Service.Command.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Command.DepartmentService
{
    public class DepartmentCommandAdapter : ICommandAdapter<DepartmentViewModel, DepartmentModel>
    {
        public DepartmentModel ToMap(DepartmentViewModel source)
        {
            return new DepartmentModel
            {
                Id = source.Id,
                Name = source.Name,
                Company = new CompanyModel { Id = source.CompanyId }
            };
        }
    }
}
