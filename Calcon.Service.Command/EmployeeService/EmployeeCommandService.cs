﻿using System;
using Calcon.Component.Command.Contract.Employee;
using Calcon.Service.Command.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Command.EmployeeService
{
    public class EmployeeCommandService : IEmployeeCommandService
    {
        private readonly IEmployeeCommand _employeeCommand;
        public EmployeeCommandService(IEmployeeCommand companyCommand)
        {
            _employeeCommand = companyCommand;
        }
        public bool Insert(EmployeeViewModel model)
        {
            _employeeCommand.Create(new EmployeeCommandAdapter().ToMap(model));
            _employeeCommand.CommitChange();
            return true;
        }

        public bool Update(EmployeeViewModel model)
        {
            _employeeCommand.Edit(new EmployeeCommandAdapter().ToMap(model));
            _employeeCommand.CommitChange();
            return true;
        }

        public bool Delete(Guid id)
        {
            _employeeCommand.Delete(id);
            _employeeCommand.CommitChange();
            return true;
        }
    }
}
