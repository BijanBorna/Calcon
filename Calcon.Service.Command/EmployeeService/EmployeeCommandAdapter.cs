﻿using Calcon.ModelCalsses.Enums;
using Calcon.ModelCalsses.Models;
using Calcon.Service.Command.Contract;
using Calcon.ViewModelClasses.ViewModels;

namespace Calcon.Service.Command.EmployeeService
{
    public class EmployeeCommandAdapter : ICommandAdapter<EmployeeViewModel, EmployeeModel>
    {
        public EmployeeModel ToMap(EmployeeViewModel source)
        {
            return new EmployeeModel
            {
                Id = source.Id,
                FirstName = source.FirstName,
                LastName = source.LastName,
                Gender = (GenderModel)source.Gender,
                Birthdate = source.Birthdate,
                Tel = source.Tel,
                Mobile = source.Mobile,
                Email = source.Email,
                Department = new DepartmentModel { Id = source.DepartmentId }
            };
        }
    }
}
